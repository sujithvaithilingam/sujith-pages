(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _guard_login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./guard/login/login.component */ "./src/app/guard/login/login.component.ts");
/* harmony import */ var _customer_customer_list_customer_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customer/customer-list/customer-list.component */ "./src/app/customer/customer-list/customer-list.component.ts");
/* harmony import */ var _employee_candidate_candidate_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./employee/candidate/candidate.component */ "./src/app/employee/candidate/candidate.component.ts");
/* harmony import */ var _employee_employee_search_employee_search_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./employee/employee-search/employee-search.component */ "./src/app/employee/employee-search/employee-search.component.ts");
/* harmony import */ var _employee_employees_employees_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./employee/employees/employees.component */ "./src/app/employee/employees/employees.component.ts");
/* harmony import */ var _employee_job_application_list_job_application_list_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./employee/job-application-list/job-application-list.component */ "./src/app/employee/job-application-list/job-application-list.component.ts");
/* harmony import */ var _employee_project_employees_project_employees_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./employee/project-employees/project-employees.component */ "./src/app/employee/project-employees/project-employees.component.ts");
/* harmony import */ var _employee_employee_details_employee_details_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./employee/employee-details/employee-details.component */ "./src/app/employee/employee-details/employee-details.component.ts");
/* harmony import */ var _guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./guard/auth/auth.guard */ "./src/app/guard/auth/auth.guard.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _customer_customers_customers_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./customer/customers/customers.component */ "./src/app/customer/customers/customers.component.ts");
/* harmony import */ var _customer_opportunity_new_opportunity_new_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./customer/opportunity-new/opportunity-new.component */ "./src/app/customer/opportunity-new/opportunity-new.component.ts");
/* harmony import */ var _customer_projects_projects_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./customer/projects/projects.component */ "./src/app/customer/projects/projects.component.ts");
/* harmony import */ var _customer_task_task_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./customer/task/task.component */ "./src/app/customer/task/task.component.ts");
/* harmony import */ var _customer_task_template_task_template_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./customer/task-template/task-template.component */ "./src/app/customer/task-template/task-template.component.ts");
/* harmony import */ var _organization_enquiry_enquiry_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./organization/enquiry/enquiry.component */ "./src/app/organization/enquiry/enquiry.component.ts");
/* harmony import */ var _organization_job_setup_job_setup_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./organization/job-setup/job-setup.component */ "./src/app/organization/job-setup/job-setup.component.ts");
/* harmony import */ var _organization_look_up_look_up_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./organization/look-up/look-up.component */ "./src/app/organization/look-up/look-up.component.ts");
/* harmony import */ var _organization_menus_menus_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./organization/menus/menus.component */ "./src/app/organization/menus/menus.component.ts");
/* harmony import */ var _organization_notification_notification_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./organization/notification/notification.component */ "./src/app/organization/notification/notification.component.ts");
/* harmony import */ var _organization_pay_group_pay_group_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./organization/pay-group/pay-group.component */ "./src/app/organization/pay-group/pay-group.component.ts");
/* harmony import */ var _organization_permission_permission_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./organization/permission/permission.component */ "./src/app/organization/permission/permission.component.ts");
/* harmony import */ var _organization_roles_roles_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./organization/roles/roles.component */ "./src/app/organization/roles/roles.component.ts");
/* harmony import */ var _organization_staff_staff_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./organization/staff/staff.component */ "./src/app/organization/staff/staff.component.ts");
/* harmony import */ var _organization_users_users_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./organization/users/users.component */ "./src/app/organization/users/users.component.ts");
/* harmony import */ var _customer_task_details_task_details_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./customer/task-details/task-details.component */ "./src/app/customer/task-details/task-details.component.ts");
/* harmony import */ var _employee_candidate_candidate_process_candidate_process_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./employee/candidate/candidate-process/candidate-process.component */ "./src/app/employee/candidate/candidate-process/candidate-process.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





























var routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: _app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"] },
    { path: 'login', component: _guard_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    // Employee
    { path: 'candidate', component: _employee_candidate_candidate_component__WEBPACK_IMPORTED_MODULE_4__["CandidateComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'employee-search', component: _employee_employee_search_employee_search_component__WEBPACK_IMPORTED_MODULE_5__["EmployeeSearchComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'employees', component: _employee_employees_employees_component__WEBPACK_IMPORTED_MODULE_6__["EmployeesComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'employee-details/:RecId', component: _employee_employee_details_employee_details_component__WEBPACK_IMPORTED_MODULE_9__["EmployeeDetailsComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'job-application-list', component: _employee_job_application_list_job_application_list_component__WEBPACK_IMPORTED_MODULE_7__["JobApplicationListComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'project-employees', component: _employee_project_employees_project_employees_component__WEBPACK_IMPORTED_MODULE_8__["ProjectEmployeesComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    // Organization
    { path: 'enquiry', component: _organization_enquiry_enquiry_component__WEBPACK_IMPORTED_MODULE_17__["EnquiryComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'job-setup', component: _organization_job_setup_job_setup_component__WEBPACK_IMPORTED_MODULE_18__["JobSetupComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'look-up', component: _organization_look_up_look_up_component__WEBPACK_IMPORTED_MODULE_19__["LookUpComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'menus', component: _organization_menus_menus_component__WEBPACK_IMPORTED_MODULE_20__["MenusComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'notification', component: _organization_notification_notification_component__WEBPACK_IMPORTED_MODULE_21__["NotificationComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'pay-group', component: _organization_pay_group_pay_group_component__WEBPACK_IMPORTED_MODULE_22__["PayGroupComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'permission', component: _organization_permission_permission_component__WEBPACK_IMPORTED_MODULE_23__["PermissionComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'roles', component: _organization_roles_roles_component__WEBPACK_IMPORTED_MODULE_24__["RolesComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'staff', component: _organization_staff_staff_component__WEBPACK_IMPORTED_MODULE_25__["StaffComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'users', component: _organization_users_users_component__WEBPACK_IMPORTED_MODULE_26__["UsersComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    // Customers
    { path: 'customers', component: _customer_customers_customers_component__WEBPACK_IMPORTED_MODULE_12__["CustomersComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'customer-list', component: _customer_customer_list_customer_list_component__WEBPACK_IMPORTED_MODULE_3__["CustomerListComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'opportunity-new', component: _customer_opportunity_new_opportunity_new_component__WEBPACK_IMPORTED_MODULE_13__["OpportunityNewComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'projects', component: _customer_projects_projects_component__WEBPACK_IMPORTED_MODULE_14__["ProjectsComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'task', component: _customer_task_task_component__WEBPACK_IMPORTED_MODULE_15__["TaskComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'task-details/:RecId', component: _customer_task_details_task_details_component__WEBPACK_IMPORTED_MODULE_27__["TaskDetailsComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'task-template', component: _customer_task_template_task_template_component__WEBPACK_IMPORTED_MODULE_16__["TaskTemplateComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'candidate-process/:RecId', component: _employee_candidate_candidate_process_candidate_process_component__WEBPACK_IMPORTED_MODULE_28__["CandidateProcessComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
    { path: 'customer-list', component: _customer_customer_list_customer_list_component__WEBPACK_IMPORTED_MODULE_3__["CustomerListComponent"], canActivate: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            providers: [_guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]],
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-admin-navigation-bar></app-admin-navigation-bar> -->\r\n\r\n\r\n<!-- \r\n<ng-template  *ngIf=\"true; else elseBlock\">zxczxc\r\n    <app-login></app-login>\r\n</ng-template>\r\n\r\n<ng-template #elseBlock>\r\n    <app-admin-template></app-admin-template>\r\n</ng-template> -->\r\n\r\n    <!-- <ng-container *ngIf=\"islogin; else elseblock\" >\r\n            <app-login></app-login>\r\n    </ng-container>\r\n    <ng-template #elseblock>\r\n            <app-admin-template></app-admin-template>\r\n    </ng-template> -->\r\n\r\n    <ng-container *ngIf=\"globalService.globalScope.isLogedIn; else elseblock\" >\r\n            <app-admin-template></app-admin-template>\r\n    </ng-container>\r\n    <ng-template #elseblock>\r\n            <app-login></app-login>\r\n    </ng-template>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _guard_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./guard/auth-service/auth.service */ "./src/app/guard/auth-service/auth.service.ts");
/* harmony import */ var _guard_global_service_global_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./guard/global-service/global-service.service */ "./src/app/guard/global-service/global-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = /** @class */ (function () {
    function AppComponent(router, authService, globalService) {
        this.router = router;
        this.authService = authService;
        this.globalService = globalService;
        this.title = 'remote-work';
        this.islogin = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.islogin = this.authService.IsLogedIn();
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _guard_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _guard_global_service_global_service_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServiceService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ag_grid_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ag-grid-angular */ "./node_modules/ag-grid-angular/main.js");
/* harmony import */ var ag_grid_angular__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ag_grid_angular__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _guard_material_material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./guard/material/material.module */ "./src/app/guard/material/material.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _guard_admin_template_admin_template_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./guard/admin-template/admin-template.component */ "./src/app/guard/admin-template/admin-template.component.ts");
/* harmony import */ var _guard_login_login_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./guard/login/login.component */ "./src/app/guard/login/login.component.ts");
/* harmony import */ var _customer_customer_list_customer_list_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./customer/customer-list/customer-list.component */ "./src/app/customer/customer-list/customer-list.component.ts");
/* harmony import */ var _customer_customer_details_customer_details_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./customer/customer-details/customer-details.component */ "./src/app/customer/customer-details/customer-details.component.ts");
/* harmony import */ var _customer_customerservice_customer_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./customer/customerservice/customer.service */ "./src/app/customer/customerservice/customer.service.ts");
/* harmony import */ var _employee_employees_employees_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./employee/employees/employees.component */ "./src/app/employee/employees/employees.component.ts");
/* harmony import */ var _employee_employee_search_employee_search_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./employee/employee-search/employee-search.component */ "./src/app/employee/employee-search/employee-search.component.ts");
/* harmony import */ var _employee_candidate_candidate_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./employee/candidate/candidate.component */ "./src/app/employee/candidate/candidate.component.ts");
/* harmony import */ var _employee_job_application_list_job_application_list_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./employee/job-application-list/job-application-list.component */ "./src/app/employee/job-application-list/job-application-list.component.ts");
/* harmony import */ var _employee_project_employees_project_employees_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./employee/project-employees/project-employees.component */ "./src/app/employee/project-employees/project-employees.component.ts");
/* harmony import */ var _employee_tools_employee_custom_cell_employee_custom_cell_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./employee/tools/employee-custom-cell/employee-custom-cell.component */ "./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.ts");
/* harmony import */ var _employee_employee_details_employee_details_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./employee/employee-details/employee-details.component */ "./src/app/employee/employee-details/employee-details.component.ts");
/* harmony import */ var _employee_tools_employee_create_popup_employee_create_popup_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./employee/tools/employee-create-popup/employee-create-popup.component */ "./src/app/employee/tools/employee-create-popup/employee-create-popup.component.ts");
/* harmony import */ var _guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./guard/auth/auth.guard */ "./src/app/guard/auth/auth.guard.ts");
/* harmony import */ var _guard_global_service_global_service_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./guard/global-service/global-service.service */ "./src/app/guard/global-service/global-service.service.ts");
/* harmony import */ var _organization_users_users_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./organization/users/users.component */ "./src/app/organization/users/users.component.ts");
/* harmony import */ var _customer_projects_projects_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./customer/projects/projects.component */ "./src/app/customer/projects/projects.component.ts");
/* harmony import */ var _customer_customers_customers_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./customer/customers/customers.component */ "./src/app/customer/customers/customers.component.ts");
/* harmony import */ var _customer_task_task_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./customer/task/task.component */ "./src/app/customer/task/task.component.ts");
/* harmony import */ var _customer_opportunity_new_opportunity_new_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./customer/opportunity-new/opportunity-new.component */ "./src/app/customer/opportunity-new/opportunity-new.component.ts");
/* harmony import */ var _customer_task_template_task_template_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./customer/task-template/task-template.component */ "./src/app/customer/task-template/task-template.component.ts");
/* harmony import */ var _organization_look_up_look_up_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./organization/look-up/look-up.component */ "./src/app/organization/look-up/look-up.component.ts");
/* harmony import */ var _organization_pay_group_pay_group_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./organization/pay-group/pay-group.component */ "./src/app/organization/pay-group/pay-group.component.ts");
/* harmony import */ var _organization_menus_menus_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./organization/menus/menus.component */ "./src/app/organization/menus/menus.component.ts");
/* harmony import */ var _organization_roles_roles_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./organization/roles/roles.component */ "./src/app/organization/roles/roles.component.ts");
/* harmony import */ var _organization_permission_permission_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./organization/permission/permission.component */ "./src/app/organization/permission/permission.component.ts");
/* harmony import */ var _organization_staff_staff_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./organization/staff/staff.component */ "./src/app/organization/staff/staff.component.ts");
/* harmony import */ var _organization_enquiry_enquiry_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./organization/enquiry/enquiry.component */ "./src/app/organization/enquiry/enquiry.component.ts");
/* harmony import */ var _organization_notification_notification_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./organization/notification/notification.component */ "./src/app/organization/notification/notification.component.ts");
/* harmony import */ var _organization_job_setup_job_setup_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./organization/job-setup/job-setup.component */ "./src/app/organization/job-setup/job-setup.component.ts");
/* harmony import */ var _customer_tools_task_cell_task_cell_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./customer/tools/task-cell/task-cell.component */ "./src/app/customer/tools/task-cell/task-cell.component.ts");
/* harmony import */ var _customer_tools_task_popup_task_popup_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./customer/tools/task-popup/task-popup.component */ "./src/app/customer/tools/task-popup/task-popup.component.ts");
/* harmony import */ var _customer_task_details_task_details_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./customer/task-details/task-details.component */ "./src/app/customer/task-details/task-details.component.ts");
/* harmony import */ var _employee_tools_employee_details_all_tasks_employee_details_all_tasks_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./employee/tools/employee-details-all-tasks/employee-details-all-tasks.component */ "./src/app/employee/tools/employee-details-all-tasks/employee-details-all-tasks.component.ts");
/* harmony import */ var _employee_tools_employee_details_all_projects_employee_details_all_projects_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./employee/tools/employee-details-all-projects/employee-details-all-projects.component */ "./src/app/employee/tools/employee-details-all-projects/employee-details-all-projects.component.ts");
/* harmony import */ var _employee_tools_employee_details_task_from_current_project_employee_details_task_from_current_project_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component */ "./src/app/employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component.ts");
/* harmony import */ var _employee_tools_employee_details_profile_employee_details_profile_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./employee/tools/employee-details-profile/employee-details-profile.component */ "./src/app/employee/tools/employee-details-profile/employee-details-profile.component.ts");
/* harmony import */ var _employee_tools_project_employees_create_popup_project_employees_create_popup_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./employee/tools/project-employees-create-popup/project-employees-create-popup.component */ "./src/app/employee/tools/project-employees-create-popup/project-employees-create-popup.component.ts");
/* harmony import */ var _employee_candidate_candidate_create_popup_candidate_create_popup_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./employee/candidate/candidate-create-popup/candidate-create-popup.component */ "./src/app/employee/candidate/candidate-create-popup/candidate-create-popup.component.ts");
/* harmony import */ var _employee_candidate_candidate_process_candidate_process_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./employee/candidate/candidate-process/candidate-process.component */ "./src/app/employee/candidate/candidate-process/candidate-process.component.ts");
/* harmony import */ var _employee_candidate_candidate_cell_candidate_cell_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./employee/candidate/candidate-cell/candidate-cell.component */ "./src/app/employee/candidate/candidate-cell/candidate-cell.component.ts");
/* harmony import */ var _customer_customer_popup_customer_popup_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./customer/customer-popup/customer-popup.component */ "./src/app/customer/customer-popup/customer-popup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _guard_admin_template_admin_template_component__WEBPACK_IMPORTED_MODULE_10__["AdminTemplateComponent"],
                _guard_login_login_component__WEBPACK_IMPORTED_MODULE_11__["LoginComponent"],
                _customer_customer_list_customer_list_component__WEBPACK_IMPORTED_MODULE_12__["CustomerListComponent"],
                _customer_customer_details_customer_details_component__WEBPACK_IMPORTED_MODULE_13__["CustomerDetailsComponent"],
                _employee_employees_employees_component__WEBPACK_IMPORTED_MODULE_15__["EmployeesComponent"],
                _employee_employee_search_employee_search_component__WEBPACK_IMPORTED_MODULE_16__["EmployeeSearchComponent"],
                _employee_candidate_candidate_component__WEBPACK_IMPORTED_MODULE_17__["CandidateComponent"],
                _employee_job_application_list_job_application_list_component__WEBPACK_IMPORTED_MODULE_18__["JobApplicationListComponent"],
                _employee_project_employees_project_employees_component__WEBPACK_IMPORTED_MODULE_19__["ProjectEmployeesComponent"],
                _employee_tools_employee_custom_cell_employee_custom_cell_component__WEBPACK_IMPORTED_MODULE_20__["EmployeeCustomCellComponent"],
                _employee_employee_details_employee_details_component__WEBPACK_IMPORTED_MODULE_21__["EmployeeDetailsComponent"],
                _employee_tools_employee_create_popup_employee_create_popup_component__WEBPACK_IMPORTED_MODULE_22__["EmployeeCreatePopupComponent"],
                _organization_users_users_component__WEBPACK_IMPORTED_MODULE_25__["UsersComponent"],
                _customer_projects_projects_component__WEBPACK_IMPORTED_MODULE_26__["ProjectsComponent"],
                _customer_customers_customers_component__WEBPACK_IMPORTED_MODULE_27__["CustomersComponent"],
                _customer_task_task_component__WEBPACK_IMPORTED_MODULE_28__["TaskComponent"],
                _customer_opportunity_new_opportunity_new_component__WEBPACK_IMPORTED_MODULE_29__["OpportunityNewComponent"],
                _customer_task_template_task_template_component__WEBPACK_IMPORTED_MODULE_30__["TaskTemplateComponent"],
                _organization_look_up_look_up_component__WEBPACK_IMPORTED_MODULE_31__["LookUpComponent"],
                _organization_pay_group_pay_group_component__WEBPACK_IMPORTED_MODULE_32__["PayGroupComponent"],
                _organization_menus_menus_component__WEBPACK_IMPORTED_MODULE_33__["MenusComponent"],
                _organization_roles_roles_component__WEBPACK_IMPORTED_MODULE_34__["RolesComponent"],
                _organization_permission_permission_component__WEBPACK_IMPORTED_MODULE_35__["PermissionComponent"],
                _organization_staff_staff_component__WEBPACK_IMPORTED_MODULE_36__["StaffComponent"],
                _organization_enquiry_enquiry_component__WEBPACK_IMPORTED_MODULE_37__["EnquiryComponent"],
                _organization_notification_notification_component__WEBPACK_IMPORTED_MODULE_38__["NotificationComponent"],
                _organization_job_setup_job_setup_component__WEBPACK_IMPORTED_MODULE_39__["JobSetupComponent"],
                _customer_tools_task_cell_task_cell_component__WEBPACK_IMPORTED_MODULE_40__["TaskCellComponent"],
                _customer_tools_task_popup_task_popup_component__WEBPACK_IMPORTED_MODULE_41__["TaskPopupComponent"],
                _customer_task_details_task_details_component__WEBPACK_IMPORTED_MODULE_42__["TaskDetailsComponent"],
                _employee_tools_employee_details_all_tasks_employee_details_all_tasks_component__WEBPACK_IMPORTED_MODULE_43__["EmployeeDetailsAllTasksComponent"],
                _employee_tools_employee_details_all_projects_employee_details_all_projects_component__WEBPACK_IMPORTED_MODULE_44__["EmployeeDetailsAllProjectsComponent"],
                _employee_tools_employee_details_task_from_current_project_employee_details_task_from_current_project_component__WEBPACK_IMPORTED_MODULE_45__["EmployeeDetailsTaskFromCurrentProjectComponent"],
                _employee_tools_employee_details_profile_employee_details_profile_component__WEBPACK_IMPORTED_MODULE_46__["EmployeeDetailsProfileComponent"],
                _employee_tools_project_employees_create_popup_project_employees_create_popup_component__WEBPACK_IMPORTED_MODULE_47__["ProjectEmployeesCreatePopupComponent"],
                _employee_candidate_candidate_create_popup_candidate_create_popup_component__WEBPACK_IMPORTED_MODULE_48__["CandidateCreatePopupComponent"],
                _employee_candidate_candidate_process_candidate_process_component__WEBPACK_IMPORTED_MODULE_49__["CandidateProcessComponent"],
                _employee_candidate_candidate_cell_candidate_cell_component__WEBPACK_IMPORTED_MODULE_50__["CandidateCellComponent"],
                _customer_customer_popup_customer_popup_component__WEBPACK_IMPORTED_MODULE_51__["CustomerPopupComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _guard_material_material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_8__["LayoutModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                ag_grid_angular__WEBPACK_IMPORTED_MODULE_2__["AgGridModule"].withComponents([])
            ],
            providers: [
                _customer_customerservice_customer_service__WEBPACK_IMPORTED_MODULE_14__["CustomerService"],
                _guard_auth_auth_guard__WEBPACK_IMPORTED_MODULE_23__["AuthGuard"],
                _guard_global_service_global_service_service__WEBPACK_IMPORTED_MODULE_24__["GlobalServiceService"]
            ],
            entryComponents: [
                _employee_tools_employee_custom_cell_employee_custom_cell_component__WEBPACK_IMPORTED_MODULE_20__["EmployeeCustomCellComponent"],
                _customer_tools_task_cell_task_cell_component__WEBPACK_IMPORTED_MODULE_40__["TaskCellComponent"],
                _customer_tools_task_popup_task_popup_component__WEBPACK_IMPORTED_MODULE_41__["TaskPopupComponent"],
                _employee_tools_employee_create_popup_employee_create_popup_component__WEBPACK_IMPORTED_MODULE_22__["EmployeeCreatePopupComponent"],
                _employee_tools_project_employees_create_popup_project_employees_create_popup_component__WEBPACK_IMPORTED_MODULE_47__["ProjectEmployeesCreatePopupComponent"],
                _employee_candidate_candidate_create_popup_candidate_create_popup_component__WEBPACK_IMPORTED_MODULE_48__["CandidateCreatePopupComponent"],
                _employee_candidate_candidate_cell_candidate_cell_component__WEBPACK_IMPORTED_MODULE_50__["CandidateCellComponent"],
                _customer_customer_popup_customer_popup_component__WEBPACK_IMPORTED_MODULE_51__["CustomerPopupComponent"]
            ],
            bootstrap: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/customer/customer-details/customer-details.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/customer/customer-details/customer-details.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  customer-details works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/customer/customer-details/customer-details.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/customer/customer-details/customer-details.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/customer/customer-details/customer-details.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/customer/customer-details/customer-details.component.ts ***!
  \*************************************************************************/
/*! exports provided: CustomerDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerDetailsComponent", function() { return CustomerDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CustomerDetailsComponent = /** @class */ (function () {
    function CustomerDetailsComponent() {
    }
    CustomerDetailsComponent.prototype.ngOnInit = function () {
    };
    CustomerDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-customer-details',
            template: __webpack_require__(/*! ./customer-details.component.html */ "./src/app/customer/customer-details/customer-details.component.html"),
            styles: [__webpack_require__(/*! ./customer-details.component.scss */ "./src/app/customer/customer-details/customer-details.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CustomerDetailsComponent);
    return CustomerDetailsComponent;
}());



/***/ }),

/***/ "./src/app/customer/customer-list/customer-list.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/customer/customer-list/customer-list.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n      <button (click)=\"openModal()\">Open Modal 1</button>\r\n<ag-grid-angular \r\n    style=\"width: 1000px; height: 540px;\" \r\n    class=\"ag-theme-balham\"\r\n    [enableSorting]=\"true\"\r\n\r\n    (gridReady)=\"onGridReady($event)\" \r\n    (selectionChanged)=\"onSelectionChanged($event)\"\r\n    [rowData]=\"rowData\" \r\n    [columnDefs]=\"columnDefs\"\r\n\r\n    [animateRows]=\"true\" \r\n\r\n\r\n    [pagination]=\"true\"\r\n    [paginationPageSize]=\"20\"\r\n\r\n    [enableFilter]=\"true\"\r\n    [floatingFilter]=\"true\" \r\n\r\n    [rowSelection]=\"'single'\"\r\n    >\r\n</ag-grid-angular>"

/***/ }),

/***/ "./src/app/customer/customer-list/customer-list.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/customer/customer-list/customer-list.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/customer/customer-list/customer-list.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/customer/customer-list/customer-list.component.ts ***!
  \*******************************************************************/
/*! exports provided: CustomerListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerListComponent", function() { return CustomerListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _customerservice_customer_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../customerservice/customer.service */ "./src/app/customer/customerservice/customer.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _customer_popup_customer_popup_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../customer-popup/customer-popup.component */ "./src/app/customer/customer-popup/customer-popup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CustomerListComponent = /** @class */ (function () {
    function CustomerListComponent(customerService, snacbar, dialog) {
        this.customerService = customerService;
        this.snacbar = snacbar;
        this.dialog = dialog;
        this.columnDefs = [
            { headerName: 'CustomerId', field: 'CustomerId' },
            { headerName: 'FirstName ', field: 'FirstName' },
            { headerName: 'Nationality', field: 'Nationality' },
            { headerName: 'Dateofbirth', field: 'Dateofbirth' },
            { headerName: 'Gender ', field: 'Gender' },
            { headerName: 'CustomerStatus ', field: 'CustomerStatus' },
            { headerName: 'Email ', field: 'Email' },
            { headerName: 'MobilePhone ', field: 'MobilePhone' },
        ];
    }
    CustomerListComponent.prototype.ngOnInit = function () {
        this.GetCustomerList();
    };
    CustomerListComponent.prototype.GetCustomerList = function () {
        var _this = this;
        debugger;
        this.customerService.GetCustomerList()
            .subscribe(function (data) {
            debugger;
            _this.rowData = data;
        });
    };
    CustomerListComponent.prototype.onGridReady = function (params) {
        var _this = this;
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.customerService.GetCustomerList()
            .subscribe(function (data) {
            debugger;
            _this.rowData = data;
        });
        console.log(params);
    };
    CustomerListComponent.prototype.onSelectionChanged = function (record) {
        var selectedData = this.gridApi.getSelectedRows();
        console.log(selectedData);
        if (selectedData) {
            debugger;
            this.snacbar.open(selectedData[0].FirstName, 'close', { duration: 700 });
        }
    };
    CustomerListComponent.prototype.openModal = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_customer_popup_customer_popup_component__WEBPACK_IMPORTED_MODULE_3__["CustomerPopupComponent"], {});
        dialogRef.afterClosed().subscribe(function (data) {
            _this.customerService.CustomerInsert(data)
                .subscribe(function (response) {
                debugger;
                alert(response);
            });
        });
    };
    CustomerListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-customer-list',
            template: __webpack_require__(/*! ./customer-list.component.html */ "./src/app/customer/customer-list/customer-list.component.html"),
            styles: [__webpack_require__(/*! ./customer-list.component.scss */ "./src/app/customer/customer-list/customer-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_customerservice_customer_service__WEBPACK_IMPORTED_MODULE_1__["CustomerService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], CustomerListComponent);
    return CustomerListComponent;
}());



/***/ }),

/***/ "./src/app/customer/customer-popup/customer-popup.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/customer/customer-popup/customer-popup.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card id=\"customerpopup\">\r\n    <h3>Customer Create</h3>\r\n    <mat-divider></mat-divider>\r\n    <mat-divider></mat-divider><br>\r\n  \r\n    <form #Customer=\"ngForm\" class=\"example-form\">      \r\n      <table class=\"example-full-width\" cellspacing=\"0\">\r\n        <tr>\r\n            <td>\r\n              <mat-form-field class=\"example-full-width\">\r\n              <input matInput name=\"FirstName\" [(ngModel)]=\"Customer.FirstName\" placeholder=\"First Name\">\r\n            </mat-form-field>\r\n          </td>\r\n           <td>\r\n              <mat-form-field class=\"example-full-width\">\r\n                <input matInput name=\"LastName\" [(ngModel)]=\"Customer.LastName\" placeholder=\"Last Name\">\r\n              </mat-form-field>\r\n          </td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n                <!-- <mat-form-field>\r\n                    <mat-select placeholder=\"Favorite food\" [(ngModel)]=\"selectedValue\" name=\"food\">\r\n                      <mat-option *ngFor=\"let food of NationalityDrop\" [value]=\"food.value\">\r\n                        {{food.viewValue}}\r\n                      </mat-option>\r\n                    </mat-select>\r\n                  </mat-form-field> -->\r\n              <mat-form-field class=\"example-full-width\">\r\n              <input matInput name=\"Nationality\" [(ngModel)]=\"Customer.Nationality\" placeholder=\"Nationality\">\r\n            </mat-form-field>\r\n          </td>\r\n           <td>\r\n              <mat-form-field class=\"example-full-width\">\r\n                  <input type=\"date\" name=\"Dateofbirth\" [(ngModel)]=\"Customer.Dateofbirth\" matInput placeholder=\"Date of birth\">\r\n              </mat-form-field>\r\n          </td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n              <mat-form-field class=\"example-full-width\">\r\n                  <input type=\"email\" name=\"Email\" [(ngModel)]=\"Customer.Email\" matInput placeholder=\"Email\">\r\n            </mat-form-field>\r\n          </td>\r\n           <td>\r\n              <mat-form-field class=\"example-full-width\">\r\n                  <input type=\"number\" name=\"BusinessPhone\" [(ngModel)]=\"Customer.BusinessPhone\" matInput placeholder=\"Business Phone\">\r\n              </mat-form-field>\r\n          </td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n              <mat-form-field class=\"example-full-width\">\r\n                  <input type=\"number\" name=\"AdditionalMobilePhone\" [(ngModel)]=\"Customer.AdditionalMobilePhone\" matInput placeholder=\"Additional Mobile Phone\">\r\n            </mat-form-field>\r\n          </td>\r\n           <td>\r\n              <mat-form-field class=\"example-full-width\">\r\n                  <input type=\"number\" name=\"MobilePhone\" [(ngModel)]=\"Customer.MobilePhone\" matInput placeholder=\"Mobile Phone\">\r\n              </mat-form-field>\r\n          </td>\r\n        </tr>\r\n      </table>      \r\n    </form>\r\n  \r\n  <br>\r\n    <mat-divider></mat-divider><br><br>\r\n    <button (click)=\"save(Customer)\" mat-raised-button color=\"primary\" >Save</button> &nbsp;\r\n    <button (click)=\"close()\" mat-raised-button color=\"accent\">Cancel</button>&nbsp;   \r\n  </mat-card>"

/***/ }),

/***/ "./src/app/customer/customer-popup/customer-popup.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/customer/customer-popup/customer-popup.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/customer/customer-popup/customer-popup.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/customer/customer-popup/customer-popup.component.ts ***!
  \*********************************************************************/
/*! exports provided: CustomerPopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerPopupComponent", function() { return CustomerPopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var CustomerPopupComponent = /** @class */ (function () {
    function CustomerPopupComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    CustomerPopupComponent.prototype.ngOnInit = function () {
    };
    CustomerPopupComponent.prototype.save = function (Customer) {
        this.dialogRef.close(Customer);
    };
    CustomerPopupComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    CustomerPopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-customer-popup',
            template: __webpack_require__(/*! ./customer-popup.component.html */ "./src/app/customer/customer-popup/customer-popup.component.html"),
            styles: [__webpack_require__(/*! ./customer-popup.component.scss */ "./src/app/customer/customer-popup/customer-popup.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], CustomerPopupComponent);
    return CustomerPopupComponent;
}());



/***/ }),

/***/ "./src/app/customer/customers/customers.component.html":
/*!*************************************************************!*\
  !*** ./src/app/customer/customers/customers.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  customers works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/customer/customers/customers.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/customer/customers/customers.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/customer/customers/customers.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/customer/customers/customers.component.ts ***!
  \***********************************************************/
/*! exports provided: CustomersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersComponent", function() { return CustomersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CustomersComponent = /** @class */ (function () {
    function CustomersComponent() {
    }
    CustomersComponent.prototype.ngOnInit = function () {
    };
    CustomersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-customers',
            template: __webpack_require__(/*! ./customers.component.html */ "./src/app/customer/customers/customers.component.html"),
            styles: [__webpack_require__(/*! ./customers.component.scss */ "./src/app/customer/customers/customers.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CustomersComponent);
    return CustomersComponent;
}());



/***/ }),

/***/ "./src/app/customer/customerservice/customer.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/customer/customerservice/customer.service.ts ***!
  \**************************************************************/
/*! exports provided: CustomerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerService", function() { return CustomerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CustomerService = /** @class */ (function () {
    function CustomerService(http) {
        this.http = http;
        this.BaseUri = "http://localhost:60584/";
    }
    CustomerService.prototype.GetCustomerList = function () {
        return this.http.get(this.BaseUri + "api/GetCustomerList");
    };
    CustomerService.prototype.ErrorHandler = function (error) {
        return rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(error.message || 'server error');
    };
    CustomerService.prototype.GetTaskList = function () {
        return this.http.get(this.BaseUri + 'api/GetTaskList');
    };
    CustomerService.prototype.CreateNewTask = function (Task) {
        return this.http.post(this.BaseUri + 'api/CreateTask?Task=' + Task, null);
        // return this.http.post(this.BaseUri + 'api/CreateTask',Task, {
        //   headers : new  HttpHeaders({'Content-Type': 'application/json'})
        // });
    };
    CustomerService.prototype.UpdateTask = function (Task) {
        return this.http.post(this.BaseUri + 'api/UpdateTask?Task=' + Task, null);
    };
    CustomerService.prototype.EditTask = function (Id) {
        return this.http.get(this.BaseUri + 'api/TaskEdit?Id=' + Id);
    };
    CustomerService.prototype.CustomerInsert = function (data) {
        return this.http.post(this.BaseUri + 'api/CreateCustomer', data, {});
    };
    // public DeleteByTask(RecId){
    //   return this.http.po(this.BaseUri + 'api/DeleteByTask' + RecId, )
    // }
    CustomerService.prototype.ChangeStatusForTask = function () {
        return this.http.get(this.BaseUri + 'api/ChangeStatustForTask');
    };
    CustomerService.prototype.AddMultipleAssignmentForTask = function () {
        return this.http.get(this.BaseUri + 'api/AddMultipleAssigmentForTask');
    };
    CustomerService.prototype.ProjectDropDown = function () {
        return this.http.get(this.BaseUri + 'api/GetProjectDrop');
    };
    CustomerService.prototype.ProjectEmployeeDrop = function (Id) {
        return this.http.get(this.BaseUri + 'api/GetEmployeeforProjectDrop?Id=' + Id);
    };
    CustomerService.prototype.GetLookupByTypeId = function (Id) {
        return this.http.get(this.BaseUri + 'api/GetLookupByTypeId?Id=' + Id);
    };
    CustomerService.prototype.GetTaskDetails = function (Id) {
        return this.http.get(this.BaseUri + 'api/GetTaskDetailsVM?RecId=' + Id);
    };
    CustomerService.prototype.GetTaskAssignHistory = function (Id) {
        return this.http.get(this.BaseUri + 'api/GetTaskAssignment?Id=' + Id);
    };
    CustomerService.prototype.GetTaskstatushistory = function (Id) {
        return this.http.get(this.BaseUri + 'api/GetTaskStatusHistory?Id=' + Id);
    };
    CustomerService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], CustomerService);
    return CustomerService;
}());



/***/ }),

/***/ "./src/app/customer/opportunity-new/opportunity-new.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/customer/opportunity-new/opportunity-new.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  opportunity-new works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/customer/opportunity-new/opportunity-new.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/customer/opportunity-new/opportunity-new.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/customer/opportunity-new/opportunity-new.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/customer/opportunity-new/opportunity-new.component.ts ***!
  \***********************************************************************/
/*! exports provided: OpportunityNewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OpportunityNewComponent", function() { return OpportunityNewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OpportunityNewComponent = /** @class */ (function () {
    function OpportunityNewComponent() {
    }
    OpportunityNewComponent.prototype.ngOnInit = function () {
    };
    OpportunityNewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-opportunity-new',
            template: __webpack_require__(/*! ./opportunity-new.component.html */ "./src/app/customer/opportunity-new/opportunity-new.component.html"),
            styles: [__webpack_require__(/*! ./opportunity-new.component.scss */ "./src/app/customer/opportunity-new/opportunity-new.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], OpportunityNewComponent);
    return OpportunityNewComponent;
}());



/***/ }),

/***/ "./src/app/customer/projects/projects.component.html":
/*!***********************************************************!*\
  !*** ./src/app/customer/projects/projects.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  projects works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/customer/projects/projects.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/customer/projects/projects.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/customer/projects/projects.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/customer/projects/projects.component.ts ***!
  \*********************************************************/
/*! exports provided: ProjectsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsComponent", function() { return ProjectsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProjectsComponent = /** @class */ (function () {
    function ProjectsComponent() {
    }
    ProjectsComponent.prototype.ngOnInit = function () {
    };
    ProjectsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-projects',
            template: __webpack_require__(/*! ./projects.component.html */ "./src/app/customer/projects/projects.component.html"),
            styles: [__webpack_require__(/*! ./projects.component.scss */ "./src/app/customer/projects/projects.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProjectsComponent);
    return ProjectsComponent;
}());



/***/ }),

/***/ "./src/app/customer/task-details/task-details.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/customer/task-details/task-details.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "Rec Id : {{RecId}}  &nbsp;\r\n<button mat-raised-button color=\"primary\" [matMenuTriggerFor]=\"menu\">Action</button>\r\n<mat-menu #menu=\"matMenu\">\r\n  <button mat-menu-item>Hold</button>\r\n  <button mat-menu-item>Near To Complete</button>\r\n  <button mat-menu-item>Complete</button>\r\n  <button mat-menu-item>Update Progress</button>\r\n</mat-menu> &nbsp;\r\n<button mat-raised-button color=\"primary\">Assign</button>&nbsp;\r\n<button mat-raised-button color=\"primary\">Print</button>\r\n<br><br>\r\n<mat-card >\r\n  <h3>Employee Task</h3>\r\n  <!-- <button mat-icon-button><mat-icon>create</mat-icon> </button>&nbsp;\r\n  <button mat-icon-button><mat-icon>arrow_back</mat-icon></button> -->\r\n  <hr>\r\n\r\n  <mat-card>\r\n    <h4>Task Details</h4>\r\n    <hr>\r\n    <mat-grid-list cols=\"3\" rowHeight=\"220px\">\r\n        <mat-grid-tile>\r\n          <table>\r\n            <tr><td><b>Identification</b></td></tr>\r\n            <tr>\r\n              <td>Task Id :</td>\r\n              <td>{{Header.RecId}} </td>\r\n            </tr>\r\n            <tr>\r\n                <td>Customer TaskId :</td>\r\n                <td>{{Header.ExternalTaskId}} </td>\r\n            </tr>\r\n            <tr>\r\n                <td>Project Id :</td>\r\n                <td>{{Header.ProjectId}} </td>\r\n              </tr>\r\n              <tr>\r\n                  <td>Group :</td>\r\n                  <td>{{Header.GroupEn}} </td>\r\n              </tr>\r\n              <tr>\r\n                  <td>Assigned To\t: </td>\r\n                  <td>{{Header.AssignedToEn}} </td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Priority : </td>\r\n                    <td>{{Header.PriorityEv}} </td>\r\n                </tr>\r\n                <tr><td></td></tr>\r\n          </table>\r\n        </mat-grid-tile>\r\n        <mat-grid-tile>\r\n            <table>\r\n                <tr><td><b>Relation</b></td></tr>\r\n              <tr>\r\n                <td>Target Date :</td>\r\n                <td>{{Header.TargetDate}} </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Estimated EffortDays\t:</td>\r\n                <td>{{Header.EstimatedEffortDays}} </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Estimated EffortHours :</td>\r\n                <td>{{Header.EstimatedEffortHours}} </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Estimated EffortMinutes : </td>\r\n                <td>{{Header.EstimatedEffortMinutes}} </td>\r\n              </tr>              \r\n              <tr>\r\n                <td>Scheduled Start\t:</td>\r\n                <td>{{Header.ScheduledStart}} </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Scheduled End\t: </td>\r\n                <td>{{Header.ScheduledEnd}} </td>\r\n              </tr>\r\n            </table>\r\n        </mat-grid-tile>\r\n        <mat-grid-tile>\r\n            <table>\r\n                <tr><td><b>Assignment</b></td></tr>\r\n              <tr>\r\n                <td>Actual Start : </td>\r\n                <td>{{Header.ActualStart}} </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Actual End : </td>\r\n                <td>{{Header.ActualEnd}} </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Actual CompletePercentage : </td>\r\n                <td>{{Header.CompletePercentage}}</td>\r\n              </tr>\r\n              <tr>\r\n                <td>Actual Status : </td>\r\n                <td>{{Header.Status==1?\"New\":Header.Status==2?\"Process\":Header.Status==3?\"Hold\":Header.Status==4?\"Near To Complete\":Header.Status==5?\"Complete\":\"\"}} </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Total Time : </td>\r\n                <td>{{Header.data}} </td>\r\n              </tr>\r\n              <tr>\r\n                <td>Task Index : </td>     \r\n                <td>{{Header.TaskStatusIndex == 1 ? 'Before' : Header.TaskStatusIndex == 2 ? 'OnTime' : Header.TaskStatusIndex == 3 ? 'Delay' : ''}}</td>                \r\n              </tr>              \r\n            </table>\r\n        </mat-grid-tile>\r\n      </mat-grid-list>\r\n      Title :\r\n      <mat-form-field class=\"example-full-width\">\r\n          <input matInput type=\"text\" readonly  name=\"Title\" value=\"{{Header.Title}}\" placeholder=\"Title\">\r\n      </mat-form-field>&nbsp;\r\n\r\n      Description : <mat-form-field class=\"example-full-width\">\r\n          <textarea readonly [value]='Header.Description' matInput placeholder=\"Leave a comment\"></textarea>\r\n                </mat-form-field> <br><br>\r\n\r\n    \r\n  </mat-card><br><br>\r\n\r\n  <mat-card>\r\n      <h4>Assignment</h4>\r\n    <hr>\r\n    <mat-card>\r\n        <mat-tab-group>\r\n            <mat-tab label=\"Active Employee\"> \r\n                <table class=\"table-hover\">\r\n                    <thead>\r\n                        <tr>\r\n                            <th>EmployeeId</th>\r\n                            <th>Name</th>\r\n                            <th>Valid From</th>\r\n                            <th>Valid To</th>\r\n                        </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                        <tr *ngFor=\"let item of CurrentActiveEmployees\">\r\n                            <td>{{item.AssignedTo}}</td>\r\n                            <td>{{item.AssignedToEn}}</td>\r\n                            <td>{{item.ValidFrom}}</td>\r\n                            <td>{{item.ValidTo}}</td>\r\n                          </tr>\r\n                    </tbody>\r\n                </table>    \r\n              \r\n            </mat-tab>\r\n            <mat-tab label=\"Assignment History\">\r\n                <table class=\"table-hover\">\r\n                    <thead>\r\n                        <tr>\r\n                            <th>Assigned To</th>\r\n                            <th>Complete Percentage</th>\r\n                            <th>Assigned Date</th>\r\n                            <th>Task Status</th>\r\n                        </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                        <tr *ngFor=\"let item of AssignHistory\">\r\n                            <td>{{item.AssignedToEn}}</td>\r\n                            <td>{{item.CompletePercentage}}%</td>\r\n                            <td>{{item.AssignedDate}}</td>\r\n                            <td>{{item.StatusEn}}</td>\r\n                          </tr>\r\n                    </tbody>\r\n                </table>                \r\n            </mat-tab>\r\n          </mat-tab-group>\r\n    </mat-card>\r\n  </mat-card><br><br>\r\n\r\n  <mat-card>\r\n      <h4>Status History </h4>\r\n      <hr>\r\n      <table class=\"table-hover\">\r\n          <thead>\r\n              <tr>\r\n                  <th></th>\r\n            <th>Change Date</th>\r\n            <th>Complete Percentage</th>\r\n            <th>Comments</th>\r\n              </tr>\r\n          </thead>\r\n          <tbody>\r\n              <tr *ngFor=\"let item of StatusHistory\">\r\n                  <td>{{item.StatusEn}}</td>\r\n                  <td>{{item.Datetime}}</td>\r\n                  <td>{{item.CompletePercentage}}</td>\r\n                  <td>{{item.Comments}}</td>\r\n                </tr>  \r\n          </tbody>\r\n      </table>\r\n      \r\n    </mat-card> <br><br>\r\n\r\n<!-- \r\n  <mat-card>\r\n      <h4>Comments</h4>\r\n      <hr>\r\n\r\n\r\n    </mat-card> -->\r\n</mat-card>"

/***/ }),

/***/ "./src/app/customer/task-details/task-details.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/customer/task-details/task-details.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-card {\n  margin: -16px; }\n\nmat-grid-tile {\n  background: #b6c1c5; }\n\n.description {\n  border: 1px solid #5a5858;\n  width: 32px;\n  padding: 2px 285px 31px 15px; }\n\nmat-tab {\n  size: 15px; }\n"

/***/ }),

/***/ "./src/app/customer/task-details/task-details.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/customer/task-details/task-details.component.ts ***!
  \*****************************************************************/
/*! exports provided: TaskDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskDetailsComponent", function() { return TaskDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _customerservice_customer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../customerservice/customer.service */ "./src/app/customer/customerservice/customer.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TaskDetailsComponent = /** @class */ (function () {
    function TaskDetailsComponent(router, route, customerservice) {
        this.router = router;
        this.route = route;
        this.customerservice = customerservice;
    }
    TaskDetailsComponent.prototype.ngOnInit = function () {
        // for get recid form url
        this.GetRecIdFromRouter();
        this.GetTakAssingmentHistory();
        this.GetTaskStatusHistory();
    };
    TaskDetailsComponent.prototype.GetRecIdFromRouter = function () {
        var _this = this;
        debugger;
        this.route.paramMap
            .subscribe(function (params) {
            var recid = parseInt(params.get('RecId'));
            _this.RecId = '' + recid;
            _this.customerservice.GetTaskDetails(_this.RecId)
                .subscribe(function (data) {
                _this.Header = data;
            });
        });
    };
    TaskDetailsComponent.prototype.GetTakAssingmentHistory = function () {
        var _this = this;
        debugger;
        this.customerservice.GetTaskAssignHistory(this.RecId)
            .subscribe(function (data) {
            _this.AssignHistory = data.History;
            _this.CurrentActiveEmployees = data.CurrentActive;
        });
    };
    TaskDetailsComponent.prototype.GetTaskStatusHistory = function () {
        var _this = this;
        debugger;
        this.customerservice.GetTaskstatushistory(this.RecId)
            .subscribe(function (data) {
            _this.StatusHistory = data;
        });
    };
    TaskDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-task-details',
            template: __webpack_require__(/*! ./task-details.component.html */ "./src/app/customer/task-details/task-details.component.html"),
            styles: [__webpack_require__(/*! ./task-details.component.scss */ "./src/app/customer/task-details/task-details.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _customerservice_customer_service__WEBPACK_IMPORTED_MODULE_2__["CustomerService"]])
    ], TaskDetailsComponent);
    return TaskDetailsComponent;
}());



/***/ }),

/***/ "./src/app/customer/task-template/task-template.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/customer/task-template/task-template.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  task-template works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/customer/task-template/task-template.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/customer/task-template/task-template.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/customer/task-template/task-template.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/customer/task-template/task-template.component.ts ***!
  \*******************************************************************/
/*! exports provided: TaskTemplateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskTemplateComponent", function() { return TaskTemplateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TaskTemplateComponent = /** @class */ (function () {
    function TaskTemplateComponent() {
    }
    TaskTemplateComponent.prototype.ngOnInit = function () {
    };
    TaskTemplateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-task-template',
            template: __webpack_require__(/*! ./task-template.component.html */ "./src/app/customer/task-template/task-template.component.html"),
            styles: [__webpack_require__(/*! ./task-template.component.scss */ "./src/app/customer/task-template/task-template.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TaskTemplateComponent);
    return TaskTemplateComponent;
}());



/***/ }),

/***/ "./src/app/customer/task/task.component.html":
/*!***************************************************!*\
  !*** ./src/app/customer/task/task.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>Task List</h3>\r\n<div> \r\n    <div class=\"button-row\">\r\n        <button (click)=\"CreatClick()\" mat-mini-fab color=\"primary\"><mat-icon>create_new_folder</mat-icon></button>\r\n        <button mat-mini-fab color=\"warn\"><mat-icon>delete</mat-icon></button>\r\n        <button mat-mini-fab color=\"primary\"><mat-icon>search</mat-icon></button>\r\n        <button mat-mini-fab color=\"primary\"><mat-icon>cached</mat-icon></button>       \r\n        <button mat-mini-fab color=\"primary\"><mat-icon >menu</mat-icon></button>\r\n        \r\n      </div>\r\n</div>\r\n<hr>\r\n<mat-card class=\"grid-matcard\">\r\n\r\n  <ag-grid-angular \r\n      style=\"width: 100%; height: 450px;\" \r\n      [gridOptions]=\"gridOptions\"\r\n\r\n      class=\"ag-theme-balham\"\r\n      [enableSorting]=\"true\"\r\n  \r\n      (gridReady)=\"onGridReady($event)\" \r\n      [rowData]=\"rowData\" \r\n      [columnDefs]=\"columnDefs\"\r\n      [animateRows]=\"true\" \r\n      \r\n      [pagination]=\"true\"\r\n      [paginationPageSize]=\"15\"\r\n  \r\n      [enableFilter]=\"true\"\r\n      [floatingFilter]=\"true\" \r\n  \r\n      [rowSelection]=\"'multiple'\"\r\n      >\r\n  </ag-grid-angular>\r\n</mat-card>\r\n"

/***/ }),

/***/ "./src/app/customer/task/task.component.scss":
/*!***************************************************!*\
  !*** ./src/app/customer/task/task.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".button-row {\n  margin-right: 0; }\n\n.grid-matcard {\n  padding: 8px; }\n"

/***/ }),

/***/ "./src/app/customer/task/task.component.ts":
/*!*************************************************!*\
  !*** ./src/app/customer/task/task.component.ts ***!
  \*************************************************/
/*! exports provided: TaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskComponent", function() { return TaskComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _tools_task_cell_task_cell_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../tools/task-cell/task-cell.component */ "./src/app/customer/tools/task-cell/task-cell.component.ts");
/* harmony import */ var _tools_task_popup_task_popup_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../tools/task-popup/task-popup.component */ "./src/app/customer/tools/task-popup/task-popup.component.ts");
/* harmony import */ var _customerservice_customer_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../customerservice/customer.service */ "./src/app/customer/customerservice/customer.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TaskComponent = /** @class */ (function () {
    function TaskComponent(http, snackBar, router, dialog, customerService) {
        this.http = http;
        this.snackBar = snackBar;
        this.router = router;
        this.dialog = dialog;
        this.customerService = customerService;
        this.gridOptions = {
            context: {
                componentParent: this
            }
        };
        this.columnDefs = [
            { headerName: 'TaskId', field: 'TaskId', cellRendererFramework: _tools_task_cell_task_cell_component__WEBPACK_IMPORTED_MODULE_4__["TaskCellComponent"], checkboxSelection: true },
            { headerName: 'Title ', field: 'Title' },
            { headerName: 'Description', field: 'Description' },
            { headerName: 'Group', field: 'GroupEn' },
            { headerName: 'Assigned To', field: 'AssignedTo' },
            { headerName: 'Task Assign Employees', field: 'TaskAssignEmployees' },
            { headerName: 'Task Assign Employee Names', field: 'TaskAssignEmployeeNames' },
            { headerName: 'Priority', field: 'PriorityEv' },
            { headerName: 'Target Date', field: 'TargetDate' },
            { headerName: 'Estimated Effort Days', field: 'EstimatedEffortDays' },
            { headerName: 'Estimated Effort Hours', field: 'EstimatedEffortHours' },
            { headerName: 'Estimated Effort Minutes', field: 'EstimatedEffortMinutes' },
            { headerName: 'Scheduled Start', field: 'ScheduledStart' },
            { headerName: 'Scheduled End', field: 'ScheduledEnd' },
            { headerName: 'Actual Start', field: 'ActualStart' },
            { headerName: 'Actual End', field: 'ActualEnd' },
            { headerName: 'Complete Percentage', field: 'CompletePercentage' },
            { headerName: 'Status', field: 'StatusEn' }
        ];
    }
    TaskComponent.prototype.ngOnInit = function () {
    };
    TaskComponent.prototype.onGridReady = function (params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.GetTaskList();
    };
    TaskComponent.prototype.GetTaskList = function () {
        var _this = this;
        this.customerService.GetTaskList()
            .subscribe(function (data) {
            _this.rowData = data;
        });
    };
    TaskComponent.prototype.onSelectionChanged = function () {
        debugger;
        var selectedData = this.gridApi.getSelectedRows();
        return selectedData;
    };
    TaskComponent.prototype.SelectedRow = function (int) {
        this.router.navigate(['/employee-details']);
    };
    TaskComponent.prototype.TaskDetails = function (data) {
        debugger;
        console.log(data);
        this.router.navigate(['/task-details', data.RecId]);
    };
    TaskComponent.prototype.EditTask = function (rowdata) {
        var _this = this;
        debugger;
        if (rowdata.RecId) {
            debugger;
            var recid_1 = rowdata.RecId;
            this.customerService.EditTask(recid_1)
                .subscribe(function (data) {
                if (data) {
                    var dialogRef = _this.dialog.open(_tools_task_popup_task_popup_component__WEBPACK_IMPORTED_MODULE_5__["TaskPopupComponent"], { data: data });
                    dialogRef.afterClosed().subscribe(function (result) {
                        debugger;
                        var assignuser = [result.AssignTo];
                        var myobj = {
                            ProjectId: result.Project,
                            AssignedTo: assignuser,
                            Priority: result.Priority,
                            Group: result.Group,
                            ScheduledStart: result.ScheduleStart,
                            ScheduledEnd: result.ScheduleEnd,
                            EstimatedEffortDays: result.Days,
                            EstimatedEffortHours: result.Hours,
                            EstimatedEffortMinutes: result.Minutes,
                            Title: result.Title,
                            Description: result.Description,
                            RecId: recid_1
                        };
                        var Task = JSON.stringify(myobj);
                        _this.customerService.UpdateTask(Task)
                            .subscribe(function (data) {
                            if (data.status) {
                                _this.snackBar.open(data.msg, 'close', { duration: 2000 });
                            }
                        });
                    });
                }
            });
        }
    };
    TaskComponent.prototype.CreatClick = function () {
        var _this = this;
        debugger;
        var dialogRef = this.dialog.open(_tools_task_popup_task_popup_component__WEBPACK_IMPORTED_MODULE_5__["TaskPopupComponent"], {});
        dialogRef.afterClosed().subscribe(function (result) {
            debugger;
            var assignuser = [result.AssignTo];
            var myobj = {
                ProjectId: result.Project,
                AssignedTo: assignuser,
                Priority: result.Priority,
                Group: result.Group,
                ScheduledStart: result.ScheduleStart,
                ScheduledEnd: result.ScheduleEnd,
                EstimatedEffortDays: result.Days,
                EstimatedEffortHours: result.Hours,
                EstimatedEffortMinutes: result.Minutes,
                Title: result.Title,
                Description: result.Description
            };
            var Task = JSON.stringify(myobj);
            _this.customerService.CreateNewTask(Task)
                .subscribe(function (data) {
                if (data.status) {
                    _this.snackBar.open(data.msg, 'close', { duration: 2000 });
                }
            });
            ///CREATE TASK
            console.log(result);
        });
    };
    TaskComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-task',
            template: __webpack_require__(/*! ./task.component.html */ "./src/app/customer/task/task.component.html"),
            styles: [__webpack_require__(/*! ./task.component.scss */ "./src/app/customer/task/task.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _customerservice_customer_service__WEBPACK_IMPORTED_MODULE_6__["CustomerService"]])
    ], TaskComponent);
    return TaskComponent;
}());



/***/ }),

/***/ "./src/app/customer/tools/task-cell/task-cell.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/customer/tools/task-cell/task-cell.component.ts ***!
  \*****************************************************************/
/*! exports provided: TaskCellComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskCellComponent", function() { return TaskCellComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TaskCellComponent = /** @class */ (function () {
    function TaskCellComponent() {
    }
    TaskCellComponent.prototype.agInit = function (params) {
        this.paramsApi = params;
        this.data = params.value;
    };
    TaskCellComponent.prototype.ngOnInit = function () {
    };
    TaskCellComponent.prototype.TaskDetails = function () {
        this.paramsApi.context.componentParent.TaskDetails(this.paramsApi.data);
    };
    TaskCellComponent.prototype.EditTask = function () {
        this.paramsApi.context.componentParent.EditTask(this.paramsApi.data);
    };
    TaskCellComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-task-cell',
            template: " \n  <mat-icon (click)=\"EditTask()\">edit</mat-icon> &nbsp;\n  <mat-icon (click)=\"TaskDetails()\">assignment</mat-icon>\n  ",
            styles: ["\n  \n  "]
        }),
        __metadata("design:paramtypes", [])
    ], TaskCellComponent);
    return TaskCellComponent;
}());



/***/ }),

/***/ "./src/app/customer/tools/task-popup/task-popup.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/customer/tools/task-popup/task-popup.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<mat-card>\r\n    <h3>Task Create</h3>\r\n    <mat-divider></mat-divider>\r\n    <mat-divider></mat-divider><br>\r\n\r\n    <form #Task=\"ngForm\" class=\"example-form\">\r\n\r\n            <mat-form-field>\r\n                <mat-select placeholder=\"Select Projects\" [(ngModel)]=\"Task.Project\" (ngModelChange)=\"Getprojectemployee($event)\" name=\"Project\">\r\n                  <mat-option *ngFor=\"let item of projectdrop\" [value]=\"item.Id\">\r\n                    {{item.Desc}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field> &nbsp;\r\n\r\n              <mat-form-field>\r\n                <mat-select placeholder=\"Assign Employee\" [(ngModel)]=\"Task.AssignTo\" name=\"AssignTo\">\r\n                  <mat-option *ngFor=\"let item of projectEmployeedrop\" [value]=\"item.Id\">\r\n                    {{item.Desc}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field> &nbsp;\r\n\r\n              <mat-form-field>\r\n                <mat-select placeholder=\"Priority\" [(ngModel)]=\"Task.Priority\" name=\"Priority\">\r\n                  <mat-option *ngFor=\"let item of PriorityDrop\" [value]=\"item.Id\">\r\n                    {{item.Desc}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>      <br>\r\n                     \r\n              <mat-form-field> \r\n                  <mat-select placeholder=\"Group\" [(ngModel)]=\"Task.Group\" name=\"Group\">\r\n                    <mat-option *ngFor=\"let item of GroupDrop\" [value]=\"item.Id\">\r\n                      {{item.Desc}}\r\n                    </mat-option>\r\n                  </mat-select>\r\n              </mat-form-field> &nbsp;\r\n\r\n              <mat-form-field class=\"example-full-width\">\r\n                  <input matInput type=\"date\"  name=\"ScheduleStart\" [(ngModel)]=\"Task.ScheduleStart\" placeholder=\"Schedule Start Date\">\r\n              </mat-form-field>&nbsp;\r\n\r\n              <mat-form-field>\r\n                <input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\">\r\n                <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n                <mat-datepicker #picker></mat-datepicker>\r\n              </mat-form-field> <br>\r\n\r\n             <mat-form-field class=\"example-full-width\">\r\n                <input matInput type=\"date\"  name=\"ScheduleEnd\" [(ngModel)]=\"Task.ScheduleEnd\" placeholder=\"Schedule End Date\">\r\n            </mat-form-field>&nbsp;\r\n\r\n            <mat-form-field class=\"example-full-width\">\r\n            <input matInput type=\"number\" name=\"Days\" [(ngModel)]=\"Task.Days\" placeholder=\"Days\">\r\n          </mat-form-field>&nbsp;\r\n\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input matInput type=\"number\" name=\"Hours\" [(ngModel)]=\"Task.Hours\" placeholder=\"Hours\">\r\n            </mat-form-field>&nbsp; <br>\r\n\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input matInput type=\"number\" name=\"Minutes\" [(ngModel)]=\"Task.Minutes\" placeholder=\"Minutes\">\r\n            </mat-form-field>&nbsp;\r\n\r\n              <mat-form-field class=\"example-full-width\">\r\n              <input matInput name=\"Title\" [(ngModel)]=\"Task.Title\" placeholder=\"Title\">\r\n            </mat-form-field>&nbsp;\r\n\r\n              <mat-form-field class=\"example-full-width\">\r\n                <textarea matInput  name=\"Description\" [(ngModel)]=\"Task.Description\" placeholder=\"Description\"></textarea>\r\n              </mat-form-field>&nbsp;\r\n              <!-- <mat-form-field class=\"example-full-width\">\r\n                 <input matInput type=\"button\" mat-button color=\"accent\" mat-mini-fab (click)=\"imgFileInput.click()\">\r\n                   <mat-icon>attachment</mat-icon>                 \r\n                   <input matInput type=\"file\" name=\"file\" #imgFileInput (change)=\"previewImage($event)\" style=\"display:none;\"/> \r\n            </mat-form-field> -->\r\n\r\n          <!-- <td>\r\n              <mat-form-field class=\"example-full-width\">\r\n                <input matInput type=\"file\" name=\"Taskfile\" [(ngModel)]=\"Task.Minutes\" placeholder=\"Document\">\r\n              </mat-form-field>\r\n          </td> -->\r\n\r\n    </form>\r\n\r\n  <br>\r\n    <mat-divider></mat-divider><br><br>\r\n    <button (click)=\"save(Task)\" mat-raised-button color=\"primary\" >Save</button> &nbsp;\r\n    <button (click)=\"close()\" mat-raised-button color=\"accent\">Cancel</button>&nbsp;\r\n  </mat-card>"

/***/ }),

/***/ "./src/app/customer/tools/task-popup/task-popup.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/customer/tools/task-popup/task-popup.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/customer/tools/task-popup/task-popup.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/customer/tools/task-popup/task-popup.component.ts ***!
  \*******************************************************************/
/*! exports provided: TaskPopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskPopupComponent", function() { return TaskPopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _customerservice_customer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../customerservice/customer.service */ "./src/app/customer/customerservice/customer.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var TaskPopupComponent = /** @class */ (function () {
    function TaskPopupComponent(customerService, dialogRef, data) {
        this.customerService = customerService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.projectdrop = [];
        this.projectEmployeedrop = [];
        this.PriorityDrop = [];
        this.GroupDrop = [];
    }
    TaskPopupComponent.prototype.GetProjectDrop = function () {
        var _this = this;
        this.customerService.ProjectDropDown()
            .subscribe(function (data) {
            _this.projectdrop = data;
        });
    };
    TaskPopupComponent.prototype.ngOnInit = function () {
        this.GetProjectDrop();
        this.GetPriorityDrop();
        this.GetGroupDrop();
    };
    TaskPopupComponent.prototype.save = function (Task) {
        this.dialogRef.close(Task);
    };
    TaskPopupComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    TaskPopupComponent.prototype.Getprojectemployee = function (event) {
        var _this = this;
        debugger;
        this.customerService.ProjectEmployeeDrop(event)
            .subscribe(function (data) {
            _this.projectEmployeedrop = data;
        });
    };
    TaskPopupComponent.prototype.GetPriorityDrop = function () {
        var _this = this;
        var Id = 6;
        this.customerService.GetLookupByTypeId(Id)
            .subscribe(function (data) {
            _this.PriorityDrop = data;
        });
    };
    TaskPopupComponent.prototype.GetGroupDrop = function () {
        var _this = this;
        var Id = 4;
        this.customerService.GetLookupByTypeId(Id)
            .subscribe(function (data) {
            _this.GroupDrop = data;
        });
    };
    TaskPopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-task-popup',
            template: __webpack_require__(/*! ./task-popup.component.html */ "./src/app/customer/tools/task-popup/task-popup.component.html"),
            styles: [__webpack_require__(/*! ./task-popup.component.scss */ "./src/app/customer/tools/task-popup/task-popup.component.scss")]
        }),
        __param(2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_customerservice_customer_service__WEBPACK_IMPORTED_MODULE_2__["CustomerService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], TaskPopupComponent);
    return TaskPopupComponent;
}());



/***/ }),

/***/ "./src/app/employee/candidate/candidate-cell/candidate-cell.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/employee/candidate/candidate-cell/candidate-cell.component.ts ***!
  \*******************************************************************************/
/*! exports provided: CandidateCellComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidateCellComponent", function() { return CandidateCellComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CandidateCellComponent = /** @class */ (function () {
    function CandidateCellComponent() {
    }
    CandidateCellComponent.prototype.agInit = function (params) {
        this.paramsApi = params;
        this.data = params.value;
    };
    CandidateCellComponent.prototype.ngOnInit = function () {
    };
    CandidateCellComponent.prototype.CandidateDetails = function () {
        this.paramsApi.context.componentParent.CandidateProcess(this.paramsApi.data);
    };
    CandidateCellComponent.prototype.EditCandidate = function () {
        this.paramsApi.context.componentParent.CreateCandidate(this.paramsApi.data);
    };
    CandidateCellComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-candidate-cell',
            template: "\n  <mat-icon (click)=\"EditCandidate()\">edit</mat-icon> &nbsp;\n  <mat-icon (click)=\"CandidateDetails()\">assignment</mat-icon>\n  ",
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], CandidateCellComponent);
    return CandidateCellComponent;
}());



/***/ }),

/***/ "./src/app/employee/candidate/candidate-create-popup/candidate-create-popup.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/employee/candidate/candidate-create-popup/candidate-create-popup.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  candidate-create-popup works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/employee/candidate/candidate-create-popup/candidate-create-popup.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/employee/candidate/candidate-create-popup/candidate-create-popup.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/candidate/candidate-create-popup/candidate-create-popup.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/employee/candidate/candidate-create-popup/candidate-create-popup.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: CandidateCreatePopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidateCreatePopupComponent", function() { return CandidateCreatePopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CandidateCreatePopupComponent = /** @class */ (function () {
    function CandidateCreatePopupComponent() {
    }
    CandidateCreatePopupComponent.prototype.ngOnInit = function () {
    };
    CandidateCreatePopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-candidate-create-popup',
            template: __webpack_require__(/*! ./candidate-create-popup.component.html */ "./src/app/employee/candidate/candidate-create-popup/candidate-create-popup.component.html"),
            styles: [__webpack_require__(/*! ./candidate-create-popup.component.scss */ "./src/app/employee/candidate/candidate-create-popup/candidate-create-popup.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CandidateCreatePopupComponent);
    return CandidateCreatePopupComponent;
}());



/***/ }),

/***/ "./src/app/employee/candidate/candidate-process/candidate-process.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/employee/candidate/candidate-process/candidate-process.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <div class=\"row\">\r\n    <div class=\"col-4\">New</div>\r\n    <div class=\"col-4\">Process</div>\r\n    <div class=\"col-4\">complete</div>\r\n  </div>\r\n</div>\r\n<mat-card>\r\n  <span style=\"font-size: 20px\">Candidate Details </span> <hr>\r\n  <div class=\"row\">\r\n    <div class=\"col-4\">\r\n      <h3>Identification</h3><hr>\r\n      <table>\r\n        <tr>\r\n          <th>Candidate Id</th>\r\n          <td>0000115</td>\r\n        </tr>\r\n        <tr>\r\n          <th>Name</th>\r\n          <td>sujith</td>\r\n        </tr>\r\n        <tr>\r\n          <th>Gender</th>\r\n          <td>0000115</td>\r\n        </tr>\r\n        <tr>\r\n          <th>National Id</th>\r\n          <td>Profession</td>\r\n        </tr>\r\n        <tr>\r\n          <th>Profession</th>\r\n          <td>0000115</td>\r\n        </tr>\r\n        <tr>\r\n          <th>Request Date</th>\r\n          <td>0000115</td>\r\n        </tr>\r\n      </table>\r\n    </div>\r\n    <div class=\"col-5 \">\r\n        <h3>Contract Details</h3><hr>\r\n        <table>\r\n          <tr>\r\n            <th>Email</th>\r\n            <td>sujith@gamil.com</td>\r\n          </tr>\r\n          <tr>\r\n            <th>Mobile Number</th>\r\n            <td>8754879040</td>\r\n          </tr> \r\n        </table>\r\n    </div>\r\n    <div class=\"col-3\">\r\n        <h3>Other Details</h3><hr>\r\n        Family Name <br>\r\n        Place Of Residence <br>\r\n        Specialization <br>\r\n        Education <br>\r\n        Mention The Trainings <br>\r\n        How Many Years <br>\r\n    </div>\r\n  </div>\r\n</mat-card><br><br>\r\n\r\n<mat-card>\r\n  <span style=\"font-size: 20px\">Status History </span>\r\n  <hr>\r\n  <table class=\" table table-striped\"> \r\n    <tr>\r\n      <th>Status</th>\r\n      <th>Change Date</th>\r\n      <th>Comments</th>\r\n    </tr>\r\n    <tr>\r\n      <td>example one data</td>\r\n      <td>example two data</td>\r\n      <td>example three data</td>\r\n    </tr>\r\n  </table>\r\n</mat-card><br><br>\r\n\r\n<mat-card>\r\n  <span style=\"font-size: 20px\">Communication Messages </span><hr>\r\n\r\n  <mat-form-field style=\"width: 400px;\">\r\n      <input matInput  placeholder=\"Title\">\r\n    </mat-form-field> <br>\r\n\r\n    <mat-form-field style=\"width : 400px;\">\r\n      <textarea matInput placeholder=\"Description\"></textarea>\r\n    </mat-form-field> <br> \r\n\r\n    Choose File : <input type=\"file\" name=\"\" id=\"\"> <br>\r\n    <button mat-raised-button color=\"primary\">Save</button>\r\n  \r\n</mat-card><br><br>"

/***/ }),

/***/ "./src/app/employee/candidate/candidate-process/candidate-process.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/employee/candidate/candidate-process/candidate-process.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/candidate/candidate-process/candidate-process.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/employee/candidate/candidate-process/candidate-process.component.ts ***!
  \*************************************************************************************/
/*! exports provided: CandidateProcessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidateProcessComponent", function() { return CandidateProcessComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CandidateProcessComponent = /** @class */ (function () {
    function CandidateProcessComponent() {
    }
    CandidateProcessComponent.prototype.ngOnInit = function () {
    };
    CandidateProcessComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-candidate-process',
            template: __webpack_require__(/*! ./candidate-process.component.html */ "./src/app/employee/candidate/candidate-process/candidate-process.component.html"),
            styles: [__webpack_require__(/*! ./candidate-process.component.scss */ "./src/app/employee/candidate/candidate-process/candidate-process.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CandidateProcessComponent);
    return CandidateProcessComponent;
}());



/***/ }),

/***/ "./src/app/employee/candidate/candidate.component.html":
/*!*************************************************************!*\
  !*** ./src/app/employee/candidate/candidate.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>Employee List </h3> \r\n\r\n<div> \r\n    <div class=\"button-row\">\r\n        <button mat-icon-button>\r\n          <mat-icon (click)=\"CreateCandidate()\">create_new_folder</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon>delete</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon >search</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon>autorenew</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon >menu</mat-icon>\r\n        </button>\r\n      </div>\r\n</div>\r\n<hr>\r\n<mat-card class=\"grid-matcard\">\r\n\r\n  <ag-grid-angular \r\n      style=\"width: 100%; height: 450px;\" \r\n      [gridOptions]=\"gridOptions\"\r\n\r\n      class=\"ag-theme-balham\"\r\n      [enableSorting]=\"true\"\r\n  \r\n      (gridReady)=\"onGridReady($event)\" \r\n      [rowData]=\"rowData\" \r\n      [columnDefs]=\"columnDefs\"\r\n      [animateRows]=\"true\" \r\n    \r\n\r\n      [pagination]=\"true\"\r\n      [paginationPageSize]=\"15\"\r\n  \r\n      [enableFilter]=\"true\"\r\n      [floatingFilter]=\"true\" \r\n  \r\n      [rowSelection]=\"'single'\"\r\n      >\r\n  </ag-grid-angular>\r\n</mat-card>\r\n"

/***/ }),

/***/ "./src/app/employee/candidate/candidate.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/employee/candidate/candidate.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/candidate/candidate.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/employee/candidate/candidate.component.ts ***!
  \***********************************************************/
/*! exports provided: CandidateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidateComponent", function() { return CandidateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _candidate_cell_candidate_cell_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./candidate-cell/candidate-cell.component */ "./src/app/employee/candidate/candidate-cell/candidate-cell.component.ts");
/* harmony import */ var _candidate_create_popup_candidate_create_popup_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./candidate-create-popup/candidate-create-popup.component */ "./src/app/employee/candidate/candidate-create-popup/candidate-create-popup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CandidateComponent = /** @class */ (function () {
    function CandidateComponent(http, snackBar, router, dialog) {
        this.http = http;
        this.snackBar = snackBar;
        this.router = router;
        this.dialog = dialog;
        this.gridOptions = {
            context: {
                componentParent: this
            }
        };
        this.columnDefs = [
            { headerName: 'User Id ', field: 'userId', cellRendererFramework: _candidate_cell_candidate_cell_component__WEBPACK_IMPORTED_MODULE_4__["CandidateCellComponent"], checkboxSelection: true },
            { headerName: 'Title ', field: 'title' },
            { headerName: 'completed', field: 'completed' },
            { headerName: '2nd title ', field: 'title' },
            { headerName: 'Title ', field: 'title' },
            { headerName: 'User Id ', field: 'userId' },
            { headerName: '4tile ', field: 'title' },
            { headerName: 'Title ', field: 'title' },
            { headerName: '6t ', field: 'title', },
        ];
    }
    CandidateComponent.prototype.ngOnInit = function () {
    };
    CandidateComponent.prototype.onGridReady = function (params) {
        var _this = this;
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.http.get('https://jsonplaceholder.typicode.com/todos').subscribe(function (d) { return _this.rowData = d; });
        console.log(params);
    };
    CandidateComponent.prototype.onSelectionChanged = function () {
        var selectedData = this.gridApi.getSelectedRows();
        if (selectedData) {
            debugger;
            this.snackBar.open(selectedData[0].title, 'close', { duration: 700 });
        }
    };
    CandidateComponent.prototype.CreateCandidate = function () {
        var createemployee = this.dialog.open(_candidate_create_popup_candidate_create_popup_component__WEBPACK_IMPORTED_MODULE_5__["CandidateCreatePopupComponent"], {});
        createemployee.afterClosed().subscribe(function (result) { return console.log(result); });
    };
    CandidateComponent.prototype.CandidateProcess = function (data) {
        // this.router.navigate(['/employee-details', data.RecId])
        this.router.navigate(['/candidate-process', 324]);
    };
    CandidateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-candidate',
            template: __webpack_require__(/*! ./candidate.component.html */ "./src/app/employee/candidate/candidate.component.html"),
            styles: [__webpack_require__(/*! ./candidate.component.scss */ "./src/app/employee/candidate/candidate.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], CandidateComponent);
    return CandidateComponent;
}());



/***/ }),

/***/ "./src/app/employee/employee-details/employee-details.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/employee/employee-details/employee-details.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "{{RecId}} <b>Employee Details</b><br><br><br>\r\n<mat-card>\r\n    <mat-tab-group>\r\n        <mat-tab label=\"Profile\"> \r\n            <app-employee-details-profile></app-employee-details-profile>\r\n        </mat-tab>\r\n        <mat-tab label=\"All Tasks\"> \r\n            <app-employee-details-all-tasks></app-employee-details-all-tasks>\r\n        </mat-tab>\r\n        <mat-tab label=\"All Projects\">\r\n            <app-employee-details-all-projects></app-employee-details-all-projects>\r\n        </mat-tab>\r\n        <mat-tab label=\"Task From Current Project\"> \r\n            <app-employee-details-task-from-current-project></app-employee-details-task-from-current-project>\r\n        </mat-tab>\r\n    </mat-tab-group>\r\n</mat-card>"

/***/ }),

/***/ "./src/app/employee/employee-details/employee-details.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/employee/employee-details/employee-details.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-card {\n  margin: -16px; }\n"

/***/ }),

/***/ "./src/app/employee/employee-details/employee-details.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/employee/employee-details/employee-details.component.ts ***!
  \*************************************************************************/
/*! exports provided: EmployeeDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeDetailsComponent", function() { return EmployeeDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmployeeDetailsComponent = /** @class */ (function () {
    function EmployeeDetailsComponent(router, route) {
        this.router = router;
        this.route = route;
    }
    EmployeeDetailsComponent.prototype.ngOnInit = function () {
        // for get recid form url
        this.GetRecIdFromRouter();
    };
    EmployeeDetailsComponent.prototype.GetRecIdFromRouter = function () {
        var _this = this;
        this.route.paramMap
            .subscribe(function (params) {
            var recid = parseInt(params.get('RecId'));
            _this.RecId = recid;
        });
    };
    EmployeeDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-details',
            template: __webpack_require__(/*! ./employee-details.component.html */ "./src/app/employee/employee-details/employee-details.component.html"),
            styles: [__webpack_require__(/*! ./employee-details.component.scss */ "./src/app/employee/employee-details/employee-details.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], EmployeeDetailsComponent);
    return EmployeeDetailsComponent;
}());



/***/ }),

/***/ "./src/app/employee/employee-search/employee-search.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/employee/employee-search/employee-search.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>Employees</h3>\r\n<hr>\r\n\r\n<mat-form-field >\r\n    <input matInput [(ngModel)]=\"Employeename\" [value]=\"Employeename\"  placeholder=\"Search Employee\" >\r\n </mat-form-field> &nbsp;&nbsp; \r\n <button mat-button  (click)=\"EmployeeSearch(Employeename)\">Go</button>\r\n<br>\r\n  \r\n<div *ngFor=\"let item of [234,22,3,4,4,5,5,6,34,5543,345,345,34,53,453,543]\">\r\n  <mat-card class=\"example-card\">\r\n    <mat-card-header>\r\n      <mat-card-title>User name {{item}}</mat-card-title>\r\n      <!-- <mat-card-subtitle>Dog Breed</mat-card-subtitle> -->\r\n    </mat-card-header>\r\n    <mat-card-content>\r\n      <b>Address</b> : {{item}}<br>\r\n      <b>Address2</b> : {{item}}\r\n    </mat-card-content>\r\n    \r\n    <mat-card-actions>\r\n        <button mat-raised-button color=\"warn\" (click)=\"GoToEmployeeDetails(item)\">Details</button> &nbsp;&nbsp;\r\n        <button mat-raised-button color=\"primary\">Chat</button>\r\n    </mat-card-actions>\r\n  </mat-card>\r\n  <br>\r\n<div>"

/***/ }),

/***/ "./src/app/employee/employee-search/employee-search.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/employee/employee-search/employee-search.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".employee-card {\n  margin-bottom: 3px; }\n"

/***/ }),

/***/ "./src/app/employee/employee-search/employee-search.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/employee/employee-search/employee-search.component.ts ***!
  \***********************************************************************/
/*! exports provided: EmployeeSearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeSearchComponent", function() { return EmployeeSearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmployeeSearchComponent = /** @class */ (function () {
    function EmployeeSearchComponent(router) {
        this.router = router;
    }
    EmployeeSearchComponent.prototype.ngOnInit = function () {
    };
    EmployeeSearchComponent.prototype.GoToEmployeeDetails = function (RecId) {
        this.router.navigate(['employee-details', RecId]);
    };
    EmployeeSearchComponent.prototype.EmployeeSearch = function (event) {
        this.Employeename = '';
        console.log(event);
    };
    EmployeeSearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-search',
            template: __webpack_require__(/*! ./employee-search.component.html */ "./src/app/employee/employee-search/employee-search.component.html"),
            styles: [__webpack_require__(/*! ./employee-search.component.scss */ "./src/app/employee/employee-search/employee-search.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], EmployeeSearchComponent);
    return EmployeeSearchComponent;
}());



/***/ }),

/***/ "./src/app/employee/employees/employees.component.html":
/*!*************************************************************!*\
  !*** ./src/app/employee/employees/employees.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>Employee List</h3>\r\n<div> \r\n    <div class=\"button-row\">\r\n        <button mat-icon-button>\r\n          <mat-icon (click)=\"CreateEmployee()\">create_new_folder</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon>delete</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon >search</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon>autorenew</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon >menu</mat-icon>\r\n        </button>\r\n      </div>\r\n</div>\r\n<hr>\r\n<mat-card class=\"grid-matcard\">\r\n\r\n  <ag-grid-angular \r\n      style=\"width: 100%; height: 450px;\" \r\n      [gridOptions]=\"gridOptions\"\r\n\r\n      class=\"ag-theme-balham\"\r\n      [enableSorting]=\"true\"\r\n  \r\n      (gridReady)=\"onGridReady($event)\" \r\n      [rowData]=\"rowData\" \r\n      [columnDefs]=\"columnDefs\"\r\n      [animateRows]=\"true\" \r\n    \r\n \r\n      [pagination]=\"true\"\r\n      [paginationPageSize]=\"15\"\r\n  \r\n      [enableFilter]=\"true\"\r\n      [floatingFilter]=\"true\" \r\n  \r\n      [rowSelection]=\"'single'\"\r\n      >\r\n  </ag-grid-angular>\r\n</mat-card>\r\n"

/***/ }),

/***/ "./src/app/employee/employees/employees.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/employee/employees/employees.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".button-row {\n  margin-right: 0; }\n\n.grid-matcard {\n  padding: 8px; }\n"

/***/ }),

/***/ "./src/app/employee/employees/employees.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/employee/employees/employees.component.ts ***!
  \***********************************************************/
/*! exports provided: EmployeesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeesComponent", function() { return EmployeesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _tools_employee_custom_cell_employee_custom_cell_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../tools/employee-custom-cell/employee-custom-cell.component */ "./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _tools_employee_create_popup_employee_create_popup_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../tools/employee-create-popup/employee-create-popup.component */ "./src/app/employee/tools/employee-create-popup/employee-create-popup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EmployeesComponent = /** @class */ (function () {
    function EmployeesComponent(http, snackBar, router, dialog) {
        this.http = http;
        this.snackBar = snackBar;
        this.router = router;
        this.dialog = dialog;
        this.gridOptions = {
            context: {
                componentParent: this
            }
        };
        this.columnDefs = [
            { headerName: 'User Id ', field: 'userId', cellRendererFramework: _tools_employee_custom_cell_employee_custom_cell_component__WEBPACK_IMPORTED_MODULE_3__["EmployeeCustomCellComponent"], checkboxSelection: true },
            { headerName: 'Title ', field: 'title' },
            { headerName: 'completed', field: 'completed' },
            { headerName: '2nd title ', field: 'title' },
            { headerName: 'Title ', field: 'title' },
            { headerName: 'User Id ', field: 'userId' },
            { headerName: '4tile ', field: 'title' },
            { headerName: 'Title ', field: 'title' },
            { headerName: '6t ', field: 'title', },
        ];
    }
    EmployeesComponent.prototype.ngOnInit = function () {
    };
    EmployeesComponent.prototype.onGridReady = function (params) {
        var _this = this;
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.http.get('https://jsonplaceholder.typicode.com/todos').subscribe(function (d) { return _this.rowData = d; });
        console.log(params);
        // Swal('Hello world!');
        // Swal({
        //   title: 'Are you sure?',
        //   text: 'You will not be able to recover this imaginary file!',
        //   type: 'warning',
        //   showCancelButton: true,
        //   confirmButtonText: 'Yes, delete it!',
        //   cancelButtonText: 'No, keep it'
        // }).then((result) => {
        //   if (result.value) {
        //     Swal(
        //       'Deleted!',
        //       'Your imaginary file has been deleted.',
        //       'success'
        //     )
        //   // For more information about handling dismissals please visit
        //   // https://sweetalert2.github.io/#handling-dismissals
        //   } else if (result.dismiss === Swal.DismissReason.cancel) {
        //     Swal(
        //       'Cancelled',
        //       'Your imaginary file is safe :)',
        //       'error'
        //     )
        //   }
        // })
    };
    EmployeesComponent.prototype.onSelectionChanged = function () {
        var selectedData = this.gridApi.getSelectedRows();
        if (selectedData) {
            debugger;
            this.snackBar.open(selectedData[0].title, 'close', { duration: 700 });
        }
    };
    EmployeesComponent.prototype.SelectedRow = function (int) {
        this.router.navigate(['/employee-details']);
    };
    EmployeesComponent.prototype.CreateEmployee = function () {
        var createemployee = this.dialog.open(_tools_employee_create_popup_employee_create_popup_component__WEBPACK_IMPORTED_MODULE_5__["EmployeeCreatePopupComponent"], {});
        createemployee.afterClosed().subscribe(function (result) { return console.log(result); });
    };
    EmployeesComponent.prototype.EditEmployee = function () {
        var dialogRef = this.dialog.open(_tools_employee_create_popup_employee_create_popup_component__WEBPACK_IMPORTED_MODULE_5__["EmployeeCreatePopupComponent"], {});
        dialogRef.afterClosed().subscribe(function (result) {
            debugger;
            console.log(result);
        });
    };
    EmployeesComponent.prototype.DetailsEmployee = function (data) {
        // this.router.navigate(['/employee-details', data.RecId])
        this.router.navigate(['/employee-details', 324]);
    };
    EmployeesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employees',
            template: __webpack_require__(/*! ./employees.component.html */ "./src/app/employee/employees/employees.component.html"),
            styles: [__webpack_require__(/*! ./employees.component.scss */ "./src/app/employee/employees/employees.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], EmployeesComponent);
    return EmployeesComponent;
}());



/***/ }),

/***/ "./src/app/employee/job-application-list/job-application-list.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/employee/job-application-list/job-application-list.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<span style=\"font-size: 20px\"> Candidate Applied Jobs</span> \r\n<div> \r\n    <div class=\"button-row\">\r\n        <button mat-icon-button>\r\n          <mat-icon>delete</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon >search</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon>autorenew</mat-icon>\r\n        </button>\r\n        <button mat-icon-button>\r\n          <mat-icon >menu</mat-icon>\r\n        </button>\r\n      </div>\r\n</div>\r\n<hr>\r\n<mat-card class=\"grid-matcard\">\r\n\r\n  <ag-grid-angular \r\n      style=\"width: 100%; height: 450px;\" \r\n      [gridOptions]=\"gridOptions\"\r\n\r\n      class=\"ag-theme-balham\"\r\n      [enableSorting]=\"true\"\r\n  \r\n      (gridReady)=\"onGridReady($event)\" \r\n      [rowData]=\"rowData\" \r\n      [columnDefs]=\"columnDefs\"\r\n      [animateRows]=\"true\" \r\n    \r\n\r\n      [pagination]=\"true\"\r\n      [paginationPageSize]=\"15\"\r\n  \r\n      [enableFilter]=\"true\"\r\n      [floatingFilter]=\"true\" \r\n  \r\n      [rowSelection]=\"'single'\"\r\n      >\r\n  </ag-grid-angular>\r\n</mat-card>\r\n"

/***/ }),

/***/ "./src/app/employee/job-application-list/job-application-list.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/employee/job-application-list/job-application-list.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".button-row {\n  margin-right: 0; }\n\n.grid-matcard {\n  padding: 8px; }\n"

/***/ }),

/***/ "./src/app/employee/job-application-list/job-application-list.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/employee/job-application-list/job-application-list.component.ts ***!
  \*********************************************************************************/
/*! exports provided: JobApplicationListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobApplicationListComponent", function() { return JobApplicationListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _tools_employee_custom_cell_employee_custom_cell_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../tools/employee-custom-cell/employee-custom-cell.component */ "./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobApplicationListComponent = /** @class */ (function () {
    function JobApplicationListComponent(http, snackBar, router, dialog) {
        this.http = http;
        this.snackBar = snackBar;
        this.router = router;
        this.dialog = dialog;
        this.gridOptions = {
            context: {
                componentParent: this
            }
        };
        this.columnDefs = [
            { headerName: 'User Id ', field: 'userId', cellRendererFramework: _tools_employee_custom_cell_employee_custom_cell_component__WEBPACK_IMPORTED_MODULE_1__["EmployeeCustomCellComponent"], checkboxSelection: true },
            { headerName: 'Title ', field: 'title' },
            { headerName: 'completed', field: 'completed' },
            { headerName: '2nd title ', field: 'title' },
            { headerName: 'Title ', field: 'title' },
            { headerName: 'User Id ', field: 'userId' },
            { headerName: '4tile ', field: 'title' },
            { headerName: 'Title ', field: 'title' },
            { headerName: '6t ', field: 'title', },
        ];
    }
    JobApplicationListComponent.prototype.ngOnInit = function () {
    };
    JobApplicationListComponent.prototype.onGridReady = function (params) {
        var _this = this;
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.http.get('https://jsonplaceholder.typicode.com/todos').subscribe(function (d) { return _this.rowData = d; });
        console.log(params);
    };
    JobApplicationListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-job-application-list',
            template: __webpack_require__(/*! ./job-application-list.component.html */ "./src/app/employee/job-application-list/job-application-list.component.html"),
            styles: [__webpack_require__(/*! ./job-application-list.component.scss */ "./src/app/employee/job-application-list/job-application-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"]])
    ], JobApplicationListComponent);
    return JobApplicationListComponent;
}());



/***/ }),

/***/ "./src/app/employee/project-employees/project-employees.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/employee/project-employees/project-employees.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\r\n    <span style=\"font-size: 20px\">Project Details</span> \r\n    <button mat-icon-button>\r\n        <mat-icon  (click)=\"ProjectEmployeeCreate()\">create_new_folder</mat-icon>\r\n      </button>\r\n    <hr>\r\n    <div>\r\n        <table>\r\n          <tr>\r\n            <th>Project Id :\t</th>\r\n            <td></td>\r\n          </tr>\r\n          <tr>\r\n            <th>Description :\t</th>\r\n            <td></td>\r\n          </tr>\r\n          <tr>\r\n            <th>Customer Id :\t</th>\r\n            <td></td>\r\n            </tr>\r\n          <tr>\r\n            <th>Name :\t</th>\r\n            <td></td>\r\n          </tr>\r\n          <tr>\r\n            <th>Start Date :\t</th>\r\n            <td></td>\r\n          </tr>\r\n          <tr>\r\n              <th>End Date :</th>\r\n              <td></td>\r\n            </tr>\r\n        </table>\r\n    </div>\r\n</mat-card><br><br>\r\n\r\n<mat-card>\r\n    <span style=\"font-size: 20px\"> Project Employee</span> \r\n    <hr>\r\n    <div> \r\n        <div class=\"button-row\">\r\n            <button mat-icon-button>\r\n                <mat-icon  (click)=\"ProjectEmployeeCreate()\">create_new_folder</mat-icon>\r\n              </button>\r\n            <button mat-icon-button>\r\n              <mat-icon>delete</mat-icon>\r\n            </button>\r\n            <button mat-icon-button>\r\n              <mat-icon >search</mat-icon>\r\n            </button>\r\n            <button mat-icon-button>\r\n              <mat-icon>autorenew</mat-icon>\r\n            </button>\r\n            <button mat-icon-button>\r\n              <mat-icon >menu</mat-icon>\r\n            </button>\r\n          </div>\r\n    </div>\r\n    <hr>\r\n    <div class=\"grid-matcard\">\r\n    \r\n      <ag-grid-angular \r\n          style=\"width: 100%; height: 450px;\" \r\n          [gridOptions]=\"gridOptions\"\r\n    \r\n          class=\"ag-theme-balham\"\r\n          [enableSorting]=\"true\"\r\n      \r\n          (gridReady)=\"onGridReady($event)\" \r\n          [rowData]=\"rowData\" \r\n          [columnDefs]=\"columnDefs\"\r\n          [animateRows]=\"true\" \r\n        \r\n    \r\n          [pagination]=\"true\"\r\n          [paginationPageSize]=\"15\"\r\n      \r\n          [enableFilter]=\"true\"\r\n          [floatingFilter]=\"true\" \r\n      \r\n          [rowSelection]=\"'single'\"\r\n          >\r\n      </ag-grid-angular>\r\n    </div>\r\n    \r\n</mat-card>"

/***/ }),

/***/ "./src/app/employee/project-employees/project-employees.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/employee/project-employees/project-employees.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/project-employees/project-employees.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/employee/project-employees/project-employees.component.ts ***!
  \***************************************************************************/
/*! exports provided: ProjectEmployeesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectEmployeesComponent", function() { return ProjectEmployeesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _tools_employee_custom_cell_employee_custom_cell_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../tools/employee-custom-cell/employee-custom-cell.component */ "./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _tools_project_employees_create_popup_project_employees_create_popup_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../tools/project-employees-create-popup/project-employees-create-popup.component */ "./src/app/employee/tools/project-employees-create-popup/project-employees-create-popup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProjectEmployeesComponent = /** @class */ (function () {
    function ProjectEmployeesComponent(http, snackBar, router, dialog) {
        this.http = http;
        this.snackBar = snackBar;
        this.router = router;
        this.dialog = dialog;
        this.gridOptions = {
            context: {
                componentParent: this
            }
        };
        this.columnDefs = [
            { headerName: 'User Id ', field: 'userId', cellRendererFramework: _tools_employee_custom_cell_employee_custom_cell_component__WEBPACK_IMPORTED_MODULE_1__["EmployeeCustomCellComponent"], checkboxSelection: true },
            { headerName: 'Title ', field: 'title' },
            { headerName: 'completed', field: 'completed' },
            { headerName: '2nd title ', field: 'title' },
            { headerName: 'Title ', field: 'title' },
            { headerName: 'User Id ', field: 'userId' },
            { headerName: '4tile ', field: 'title' },
            { headerName: 'Title ', field: 'title' },
            { headerName: '6t ', field: 'title', },
        ];
    }
    ProjectEmployeesComponent.prototype.ngOnInit = function () {
    };
    ProjectEmployeesComponent.prototype.onGridReady = function (params) {
        var _this = this;
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.http.get('https://jsonplaceholder.typicode.com/todos').subscribe(function (d) { return _this.rowData = d; });
        console.log(params);
    };
    ProjectEmployeesComponent.prototype.ProjectEmployeeCreate = function () {
        var popup = this.dialog.open(_tools_project_employees_create_popup_project_employees_create_popup_component__WEBPACK_IMPORTED_MODULE_5__["ProjectEmployeesCreatePopupComponent"], {});
        popup.afterClosed().subscribe(function (result) {
            debugger;
            console.log(result);
        });
    };
    ProjectEmployeesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-project-employees',
            template: __webpack_require__(/*! ./project-employees.component.html */ "./src/app/employee/project-employees/project-employees.component.html"),
            styles: [__webpack_require__(/*! ./project-employees.component.scss */ "./src/app/employee/project-employees/project-employees.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"]])
    ], ProjectEmployeesComponent);
    return ProjectEmployeesComponent;
}());



/***/ }),

/***/ "./src/app/employee/tools/employee-create-popup/employee-create-popup.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-create-popup/employee-create-popup.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\r\n    <h3>Employee</h3>\r\n    <mat-divider></mat-divider><br>\r\n\r\n    <form #Employee=\"ngForm\" class=\"example-form\">\r\n\r\n          <mat-form-field>\r\n            <input matInput name=\"EmployeeId\" [(ngModel)]=\"Employee.EmployeeId\" placeholder=\"Employee ID\">\r\n          </mat-form-field>&nbsp;&nbsp;\r\n\r\n          <mat-form-field>\r\n            <input matInput name=\"Name\" [(ngModel)]=\"Employee.Name\" placeholder=\"Name\">\r\n          </mat-form-field>&nbsp;&nbsp;\r\n\r\n          <mat-form-field>\r\n            <mat-select placeholder=\"Gender\" name=\"Gender\" [(ngModel)]=\"Employee.Gender\">\r\n              <mat-option  *ngFor=\"let food of ['sdf','sdfsdf', 'sdfsdfdsf','sdfsdf']\" value=\"\">{{food}}</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>&nbsp;&nbsp;\r\n\r\n          <mat-form-field>\r\n            <mat-select placeholder=\"Profession\" name=\"Profession\" [(ngModel)]=\"Employee.Profession\" >\r\n              <mat-option  *ngFor=\"let food of ['sdf','sdfsdf', 'sdfsdfdsf','sdfsdf']\" value=\"\">{{food}}</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>&nbsp;&nbsp;\r\n\r\n          <mat-form-field>\r\n            <input name=\"Contract ID\" [(ngModel)]=\"Employee.ContractId\" type=\"number\" matInput placeholder=\"Contract ID \">\r\n          </mat-form-field>&nbsp;&nbsp;\r\n\r\n          <mat-form-field>\r\n              <input name=\"ContractValidTo\" [(ngModel)]=\"Employee.ContractValidTo\" matInput [matDatepicker]=\"ContractValidTo\" placeholder=\"Contract Valid To\">\r\n              <mat-datepicker-toggle matSuffix [for]=\"ContractValidTo\"></mat-datepicker-toggle>\r\n              <mat-datepicker #ContractValidTo></mat-datepicker>\r\n          </mat-form-field> &nbsp;&nbsp;\r\n\r\n          <mat-form-field>\r\n            <input name=\"NationalId\" [(ngModel)]=\"Employee.NationalId\" matInput placeholder=\"National ID\">\r\n          </mat-form-field>&nbsp;&nbsp;\r\n\r\n          <mat-form-field>\r\n            <mat-select placeholder=\"Nationality\" name=\"Nationality\" [(ngModel)]=\"Employee.Nationality\" >\r\n              <mat-option  *ngFor=\"let food of ['sdf','sdfsdf', 'sdfsdfdsf','sdfsdf']\" value=\"\">{{food}}</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>&nbsp;&nbsp;\r\n\r\n          <mat-form-field>\r\n              <mat-select placeholder=\"Labour Type\" name=\"LabourType\" [(ngModel)]=\"Employee.LabourType\">\r\n                <mat-option  *ngFor=\"let food of ['sdf','sdfsdf', 'sdfsdfdsf','sdfsdf']\" value=\"\">{{food}}</mat-option>\r\n              </mat-select>\r\n          </mat-form-field>&nbsp;&nbsp;\r\n\r\n          <mat-form-field>\r\n                  <input matInput name=\"ContractValidFrom\" [(ngModel)]=\"Employee.ContractValidFrom\" [matDatepicker]=\"ContractValidFrom\" placeholder=\"Contract Valid To\">\r\n                  <mat-datepicker-toggle matSuffix [for]=\"ContractValidFrom\"></mat-datepicker-toggle>\r\n                  <mat-datepicker #ContractValidFrom></mat-datepicker>\r\n          </mat-form-field> &nbsp;&nbsp;\r\n\r\n    </form>\r\n\r\n  <br>\r\n    <mat-divider></mat-divider><br>\r\n    <button (click)=\"save(Employee)\" mat-raised-button color=\"primary\" >Save</button> &nbsp;\r\n    <button (click)=\"close()\" mat-raised-button color=\"accent\">close</button>&nbsp;\r\n  </mat-card>"

/***/ }),

/***/ "./src/app/employee/tools/employee-create-popup/employee-create-popup.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-create-popup/employee-create-popup.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-card {\n  margin: -17px;\n  padding: 12px; }\n\nmat-divider {\n  width: 3px; }\n\nmat-form-field {\n  width: 320px; }\n"

/***/ }),

/***/ "./src/app/employee/tools/employee-create-popup/employee-create-popup.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-create-popup/employee-create-popup.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: EmployeeCreatePopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeCreatePopupComponent", function() { return EmployeeCreatePopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var EmployeeCreatePopupComponent = /** @class */ (function () {
    function EmployeeCreatePopupComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    EmployeeCreatePopupComponent.prototype.ngOnInit = function () {
    };
    EmployeeCreatePopupComponent.prototype.save = function (data) {
        this.dialogRef.close(data);
    };
    EmployeeCreatePopupComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    EmployeeCreatePopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-create-popup',
            template: __webpack_require__(/*! ./employee-create-popup.component.html */ "./src/app/employee/tools/employee-create-popup/employee-create-popup.component.html"),
            styles: [__webpack_require__(/*! ./employee-create-popup.component.scss */ "./src/app/employee/tools/employee-create-popup/employee-create-popup.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], EmployeeCreatePopupComponent);
    return EmployeeCreatePopupComponent;
}());



/***/ }),

/***/ "./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-icon (click)=\"employeeEdit()\">edit</mat-icon> &nbsp;\r\n<mat-icon (click)=\"employeeDetails()\">assignment</mat-icon>\r\n"

/***/ }),

/***/ "./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.ts ***!
  \***************************************************************************************/
/*! exports provided: EmployeeCustomCellComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeCustomCellComponent", function() { return EmployeeCustomCellComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EmployeeCustomCellComponent = /** @class */ (function () {
    function EmployeeCustomCellComponent() {
    }
    EmployeeCustomCellComponent.prototype.agInit = function (params) {
        this.paramsApi = params;
        this.data = params.value;
    };
    EmployeeCustomCellComponent.prototype.ngOnInit = function () {
    };
    EmployeeCustomCellComponent.prototype.employeeDetails = function () {
        this.paramsApi.context.componentParent.DetailsEmployee(this.paramsApi.data);
    };
    EmployeeCustomCellComponent.prototype.employeeEdit = function () {
        this.paramsApi.context.componentParent.CreateEmployee();
    };
    EmployeeCustomCellComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-custom-cell',
            template: __webpack_require__(/*! ./employee-custom-cell.component.html */ "./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.html"),
            styles: [__webpack_require__(/*! ./employee-custom-cell.component.scss */ "./src/app/employee/tools/employee-custom-cell/employee-custom-cell.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EmployeeCustomCellComponent);
    return EmployeeCustomCellComponent;
}());



/***/ }),

/***/ "./src/app/employee/tools/employee-details-all-projects/employee-details-all-projects.component.html":
/*!***********************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-all-projects/employee-details-all-projects.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"grid-matcard\">\r\n\r\n    <ag-grid-angular \r\n        style=\"width: 100%; height: 450px;\" \r\n  \r\n        class=\"ag-theme-balham\"\r\n        [enableSorting]=\"true\"\r\n    \r\n        [rowData]=\"rowData\" \r\n        [columnDefs]=\"columnDefs\"\r\n        [animateRows]=\"true\" \r\n        \r\n        [pagination]=\"true\"\r\n        [paginationPageSize]=\"15\"\r\n    \r\n        [enableFilter]=\"true\"\r\n        [floatingFilter]=\"true\" \r\n    \r\n        [rowSelection]=\"'single'\"\r\n        >\r\n    </ag-grid-angular>\r\n  </mat-card>"

/***/ }),

/***/ "./src/app/employee/tools/employee-details-all-projects/employee-details-all-projects.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-all-projects/employee-details-all-projects.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".grid-matcard {\n  padding: 8px; }\n"

/***/ }),

/***/ "./src/app/employee/tools/employee-details-all-projects/employee-details-all-projects.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-all-projects/employee-details-all-projects.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: EmployeeDetailsAllProjectsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeDetailsAllProjectsComponent", function() { return EmployeeDetailsAllProjectsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmployeeDetailsAllProjectsComponent = /** @class */ (function () {
    function EmployeeDetailsAllProjectsComponent(http) {
        this.http = http;
        this.gridOptions = {
            context: {
                componentParent: this
            }
        };
        this.columnDefs = [
            { headerName: 'userId', field: 'userId' },
            { headerName: 'id ', field: 'id' },
            { headerName: 'title ', field: 'title' },
            { headerName: 'completed ', field: 'completed' },
        ];
    }
    EmployeeDetailsAllProjectsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.http.get('https://jsonplaceholder.typicode.com/todos')
            .subscribe(function (data) { return _this.rowData = data; });
    };
    EmployeeDetailsAllProjectsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-details-all-projects',
            template: __webpack_require__(/*! ./employee-details-all-projects.component.html */ "./src/app/employee/tools/employee-details-all-projects/employee-details-all-projects.component.html"),
            styles: [__webpack_require__(/*! ./employee-details-all-projects.component.scss */ "./src/app/employee/tools/employee-details-all-projects/employee-details-all-projects.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EmployeeDetailsAllProjectsComponent);
    return EmployeeDetailsAllProjectsComponent;
}());



/***/ }),

/***/ "./src/app/employee/tools/employee-details-all-tasks/employee-details-all-tasks.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-all-tasks/employee-details-all-tasks.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"grid-matcard\">\r\n\r\n    <ag-grid-angular \r\n        style=\"width: 100%; height: 450px;\" \r\n  \r\n        class=\"ag-theme-balham\"\r\n        [enableSorting]=\"true\"\r\n    \r\n        [rowData]=\"rowData\" \r\n        [columnDefs]=\"columnDefs\"\r\n        [animateRows]=\"true\" \r\n        \r\n        [pagination]=\"true\"\r\n        [paginationPageSize]=\"15\"\r\n    \r\n        [enableFilter]=\"true\"\r\n        [floatingFilter]=\"true\" \r\n    \r\n        [rowSelection]=\"'single'\"\r\n        >\r\n    </ag-grid-angular>\r\n  </mat-card>"

/***/ }),

/***/ "./src/app/employee/tools/employee-details-all-tasks/employee-details-all-tasks.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-all-tasks/employee-details-all-tasks.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".grid-matcard {\n  padding: 8px; }\n"

/***/ }),

/***/ "./src/app/employee/tools/employee-details-all-tasks/employee-details-all-tasks.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-all-tasks/employee-details-all-tasks.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: EmployeeDetailsAllTasksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeDetailsAllTasksComponent", function() { return EmployeeDetailsAllTasksComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmployeeDetailsAllTasksComponent = /** @class */ (function () {
    function EmployeeDetailsAllTasksComponent(http) {
        this.http = http;
        this.gridOptions = {
            context: {
                componentParent: this
            }
        };
        this.columnDefs = [
            { headerName: 'userId', field: 'userId' },
            { headerName: 'id ', field: 'id' },
            { headerName: 'title ', field: 'title' },
            { headerName: 'completed ', field: 'completed' },
        ];
    }
    EmployeeDetailsAllTasksComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.http.get('https://jsonplaceholder.typicode.com/todos')
            .subscribe(function (data) { return _this.rowData = data; });
    };
    EmployeeDetailsAllTasksComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-details-all-tasks',
            template: __webpack_require__(/*! ./employee-details-all-tasks.component.html */ "./src/app/employee/tools/employee-details-all-tasks/employee-details-all-tasks.component.html"),
            styles: [__webpack_require__(/*! ./employee-details-all-tasks.component.scss */ "./src/app/employee/tools/employee-details-all-tasks/employee-details-all-tasks.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EmployeeDetailsAllTasksComponent);
    return EmployeeDetailsAllTasksComponent;
}());



/***/ }),

/***/ "./src/app/employee/tools/employee-details-profile/employee-details-profile.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-profile/employee-details-profile.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br><br>\r\n<b>Employee Id</b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>National Id </b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Profession </b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Gender </b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Email </b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Labour Type </b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Address </b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Contact No</b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Family Name</b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Place of Residence</b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Specialization</b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Education </b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>Mention The Trainings</b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br>\r\n<b>How Many Years </b> &nbsp;:&nbsp;<span>sdfsdfsdfdsf</span> <br><br>\r\n<button  mat-raised-button color=\"primary\">Edit Profile</button>\r\n"

/***/ }),

/***/ "./src/app/employee/tools/employee-details-profile/employee-details-profile.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-profile/employee-details-profile.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/tools/employee-details-profile/employee-details-profile.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-profile/employee-details-profile.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: EmployeeDetailsProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeDetailsProfileComponent", function() { return EmployeeDetailsProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EmployeeDetailsProfileComponent = /** @class */ (function () {
    function EmployeeDetailsProfileComponent() {
    }
    EmployeeDetailsProfileComponent.prototype.ngOnInit = function () {
    };
    EmployeeDetailsProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-details-profile',
            template: __webpack_require__(/*! ./employee-details-profile.component.html */ "./src/app/employee/tools/employee-details-profile/employee-details-profile.component.html"),
            styles: [__webpack_require__(/*! ./employee-details-profile.component.scss */ "./src/app/employee/tools/employee-details-profile/employee-details-profile.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EmployeeDetailsProfileComponent);
    return EmployeeDetailsProfileComponent;
}());



/***/ }),

/***/ "./src/app/employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component.html":
/*!*************************************************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component.html ***!
  \*************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"grid-matcard\">\r\n\r\n    <ag-grid-angular \r\n        style=\"width: 100%; height: 450px;\" \r\n  \r\n        class=\"ag-theme-balham\"\r\n        [enableSorting]=\"true\"\r\n    \r\n        [rowData]=\"rowData\" \r\n        [columnDefs]=\"columnDefs\"\r\n        [animateRows]=\"true\" \r\n        \r\n        [pagination]=\"true\"\r\n        [paginationPageSize]=\"15\"\r\n    \r\n        [enableFilter]=\"true\"\r\n        [floatingFilter]=\"true\" \r\n    \r\n        [rowSelection]=\"'single'\"\r\n        >\r\n    </ag-grid-angular>\r\n  </mat-card>"

/***/ }),

/***/ "./src/app/employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component.scss":
/*!*************************************************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component.scss ***!
  \*************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".grid-matcard {\n  padding: 8px; }\n"

/***/ }),

/***/ "./src/app/employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component.ts":
/*!***********************************************************************************************************************************!*\
  !*** ./src/app/employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component.ts ***!
  \***********************************************************************************************************************************/
/*! exports provided: EmployeeDetailsTaskFromCurrentProjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeDetailsTaskFromCurrentProjectComponent", function() { return EmployeeDetailsTaskFromCurrentProjectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmployeeDetailsTaskFromCurrentProjectComponent = /** @class */ (function () {
    function EmployeeDetailsTaskFromCurrentProjectComponent(http) {
        this.http = http;
        this.gridOptions = {
            context: {
                componentParent: this
            }
        };
        this.columnDefs = [
            { headerName: 'userId', field: 'userId' },
            { headerName: 'id ', field: 'id' },
            { headerName: 'title ', field: 'title' },
            { headerName: 'completed ', field: 'completed' },
        ];
    }
    EmployeeDetailsTaskFromCurrentProjectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.http.get('https://jsonplaceholder.typicode.com/todos')
            .subscribe(function (data) { return _this.rowData = data; });
    };
    EmployeeDetailsTaskFromCurrentProjectComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-details-task-from-current-project',
            template: __webpack_require__(/*! ./employee-details-task-from-current-project.component.html */ "./src/app/employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component.html"),
            styles: [__webpack_require__(/*! ./employee-details-task-from-current-project.component.scss */ "./src/app/employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EmployeeDetailsTaskFromCurrentProjectComponent);
    return EmployeeDetailsTaskFromCurrentProjectComponent;
}());



/***/ }),

/***/ "./src/app/employee/tools/project-employees-create-popup/project-employees-create-popup.component.html":
/*!*************************************************************************************************************!*\
  !*** ./src/app/employee/tools/project-employees-create-popup/project-employees-create-popup.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<mat-card>\r\n    <h3>Project Employees</h3>\r\n    <mat-divider></mat-divider>\r\n    <mat-divider></mat-divider><br>\r\n\r\n    <form #ProjectEmployee=\"ngForm\" class=\"example-form\">\r\n\r\n            <mat-form-field>\r\n                <mat-select placeholder=\"Select Employee\" [(ngModel)]=\"ProjectEmployee.Employee\" name=\"Employee\">\r\n                  <mat-option *ngFor=\"let food of ['hyder', 'hyder 2', 'hyder 3', 'hyder 4']\" [value]=\"food\">\r\n                    {{food}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field> &nbsp;\r\n\r\n              <mat-form-field >\r\n                  <input type=\"number\" matInput name=\"NationlId\" [(ngModel)]=\"ProjectEmployee.NationlId\" placeholder=\"Nationl Id\">\r\n                </mat-form-field>&nbsp;\r\n\r\n               <mat-form-field >\r\n                  <input  matInput readonly name=\"Name\" [(ngModel)]=\"ProjectEmployee.Name\" placeholder=\"Name\">\r\n                </mat-form-field>&nbsp;\r\n  \r\n                <mat-form-field >\r\n                    <input  matInput readonly name=\"Nationality\" [(ngModel)]=\"ProjectEmployee.Nationality\" placeholder=\"Nationality\">\r\n                  </mat-form-field>&nbsp;<br>\r\n\r\n          <mat-form-field>\r\n              <input name=\"ValidFrom\" [(ngModel)]=\"ProjectEmployee.ValidFrom\" matInput [matDatepicker]=\"ValidFrom\" placeholder=\" Valid From\">\r\n              <mat-datepicker-toggle matSuffix [for]=\"ValidFrom\"></mat-datepicker-toggle>\r\n              <mat-datepicker #ValidFrom></mat-datepicker>\r\n          </mat-form-field> &nbsp;&nbsp;\r\n\r\n          <mat-form-field>\r\n              <input name=\"ValidTo\" [(ngModel)]=\"ProjectEmployee.ValidTo\" matInput [matDatepicker]=\"ValidTo\" placeholder=\"Valid To\">\r\n              <mat-datepicker-toggle matSuffix [for]=\"ValidTo\"></mat-datepicker-toggle>\r\n              <mat-datepicker #ValidTo></mat-datepicker>\r\n          </mat-form-field> &nbsp;&nbsp;\r\n    </form>\r\n\r\n  <br>\r\n    <mat-divider></mat-divider><br><br>\r\n    <button (click)=\"save(ProjectEmployee)\" mat-raised-button color=\"primary\" >Save</button> &nbsp;\r\n    <button (click)=\"close()\" mat-raised-button color=\"accent\">Cancel</button>&nbsp;\r\n  </mat-card>"

/***/ }),

/***/ "./src/app/employee/tools/project-employees-create-popup/project-employees-create-popup.component.scss":
/*!*************************************************************************************************************!*\
  !*** ./src/app/employee/tools/project-employees-create-popup/project-employees-create-popup.component.scss ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/tools/project-employees-create-popup/project-employees-create-popup.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/employee/tools/project-employees-create-popup/project-employees-create-popup.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: ProjectEmployeesCreatePopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectEmployeesCreatePopupComponent", function() { return ProjectEmployeesCreatePopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var ProjectEmployeesCreatePopupComponent = /** @class */ (function () {
    function ProjectEmployeesCreatePopupComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    ProjectEmployeesCreatePopupComponent.prototype.ngOnInit = function () {
    };
    ProjectEmployeesCreatePopupComponent.prototype.save = function (Task) {
        this.dialogRef.close(Task);
    };
    ProjectEmployeesCreatePopupComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    ProjectEmployeesCreatePopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-project-employees-create-popup',
            template: __webpack_require__(/*! ./project-employees-create-popup.component.html */ "./src/app/employee/tools/project-employees-create-popup/project-employees-create-popup.component.html"),
            styles: [__webpack_require__(/*! ./project-employees-create-popup.component.scss */ "./src/app/employee/tools/project-employees-create-popup/project-employees-create-popup.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], ProjectEmployeesCreatePopupComponent);
    return ProjectEmployeesCreatePopupComponent;
}());



/***/ }),

/***/ "./src/app/guard/admin-template/admin-template.component.html":
/*!********************************************************************!*\
  !*** ./src/app/guard/admin-template/admin-template.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\" [class.example-is-mobile]=\"mobileQuery.matches\" > \r\n\r\n  <mat-toolbar color=\"primary\"  style=\"height:40px;\">\r\n    <mat-toolbar-row>\r\n      <button mat-icon-button  (click)=\"snav.toggle()\"><mat-icon>menu</mat-icon></button>\r\n      <span>Remote Work</span>\r\n      <mat-icon class=\"example-icon\">favorite</mat-icon>\r\n      <mat-icon class=\"example-icon\">delete</mat-icon>\r\n      <mat-icon class=\"example-icon\" [matMenuTriggerFor]=\"menu\">power_settings_new</mat-icon>\r\n      <span class=\"example-spacer\"></span>\r\n      <mat-menu #menu=\"matMenu\">\r\n       <button mat-menu-item (click)=\"LogOut()\">\r\n         <mat-icon>power_settings_new</mat-icon>\r\n         <span>Logout</span>\r\n       </button>\r\n     </mat-menu>\r\n     <span  style=\"margin: 0 10px 0 10px\">{{globalService.globalScope.userName}}</span>\r\n    </mat-toolbar-row>\r\n  </mat-toolbar>\r\n\r\n\r\n\r\n  <!-- top bar -->\r\n   <!-- <mat-toolbar  style=\"height:50px; background-color: rgb(138, 137, 235)\" class=\"example-toolbar\">\r\n     <button mat-icon-button  (click)=\"snav.toggle()\"><mat-icon>menu</mat-icon></button>\r\n     <span>Remote Work</span>\r\n     <span >{{globalService.globalScope.userName}}</span>\r\n     <mat-icon  [matMenuTriggerFor]=\"menu\">power_settings_new</mat-icon>\r\n     <mat-menu #menu=\"matMenu\">\r\n       <button mat-menu-item (click)=\"LogOut()\">\r\n         <mat-icon>power_settings_new</mat-icon>\r\n         <span>Logout</span>\r\n       </button>\r\n     </mat-menu>\r\n   </mat-toolbar> -->\r\n   \r\n   <!-- side nav -->\r\n   <mat-sidenav-container color=\"primary\"  class=\"example-sidenav-container\" >\r\n     <mat-sidenav class=\"example-sidenav\" #snav mode=\"side\" opened [fixedInViewport]=\"mobileQuery.matches\" fixedTopGap=\"0\">\r\n       <mat-nav-list style=\"width: 200px;\">\r\n         <mat-expansion-panel>\r\n             <mat-expansion-panel-header  style=\"height: 35px;\">\r\n               <mat-panel-title >\r\n               Organization\r\n               </mat-panel-title>\r\n             </mat-expansion-panel-header >\r\n               <mat-nav-list class=\"exampl-mat-nav-list\">\r\n                   <a mat-list-item routerLink=\"/enquiry\" routerLinkActive=\"active\" > Enquiry</a>\r\n                   <a mat-list-item routerLink=\"/job-setup\" routerLinkActive=\"active\" > Job Setup</a>\r\n                   <a mat-list-item routerLink=\"/look-up\" routerLinkActive=\"active\" > Look Up</a>\r\n                   <a mat-list-item routerLink=\"/menus\" routerLinkActive=\"active\" > Menus</a>\r\n                   <a mat-list-item routerLink=\"/notification\" routerLinkActive=\"active\" > Notification</a>\r\n                   <a mat-list-item routerLink=\"/pay-group\" routerLinkActive=\"active\" > Pay Group</a>\r\n                   <a mat-list-item routerLink=\"/permission\" routerLinkActive=\"active\" > Permission</a>\r\n                   <a mat-list-item routerLink=\"/roles\" routerLinkActive=\"active\" > Roles</a>\r\n                   <a mat-list-item routerLink=\"/staff\" routerLinkActive=\"active\" > Staff</a>\r\n                   <a mat-list-item routerLink=\"/users\" routerLinkActive=\"active\" >Users</a>\r\n                \r\n               </mat-nav-list>\r\n        </mat-expansion-panel>\r\n\r\n         <mat-expansion-panel>\r\n             <mat-expansion-panel-header  style=\"height: 35px;\">\r\n               <mat-panel-title >\r\n                Employee\r\n               </mat-panel-title>\r\n             </mat-expansion-panel-header >\r\n               <mat-nav-list class=\"exampl-mat-nav-list\">\r\n                   <a mat-list-item routerLink=\"/employees\" routerLinkActive=\"active\" >Employees</a>\r\n                   <a mat-list-item routerLink=\"/employee-search\" routerLinkActive=\"active\" >Employee Search</a>\r\n                   <a mat-list-item routerLink=\"/candidate\" routerLinkActive=\"active\" >Candidate</a>\r\n                   <a mat-list-item routerLink=\"/job-application-list\" routerLinkActive=\"active\" >Job Application List</a>\r\n                   <a mat-list-item routerLink=\"/project-employees\" routerLinkActive=\"active\" >Project Employees</a>\r\n               </mat-nav-list>\r\n        </mat-expansion-panel>\r\n\r\n         <mat-expansion-panel>\r\n             <mat-expansion-panel-header  style=\"height: 35px;\">\r\n               <mat-panel-title >\r\n                Customer\r\n               </mat-panel-title>\r\n             </mat-expansion-panel-header >\r\n               <mat-nav-list color=\"primary\" class=\"exampl-mat-nav-list\">\r\n                  <a mat-list-item routerLink=\"/customers\" routerLinkActive=\"active\" >Customers</a>\r\n                  <a mat-list-item routerLink=\"/customer-list\" routerLinkActive=\"active\" >Customer List</a>\r\n                  <a mat-list-item routerLink=\"/opportunity-new\" routerLinkActive=\"active\" >Opportunity New </a>\r\n                  <a mat-list-item routerLink=\"/projects\" routerLinkActive=\"active\" >Projects</a>\r\n                  <a mat-list-item routerLink=\"/task\" routerLinkActive=\"active\" >Task</a>\r\n                  <a mat-list-item routerLink=\"/task-template\" routerLinkActive=\"active\" >Task Template</a>\r\n\r\n\r\n                   <!-- <a mat-list-item routerLink=\"/customer-list\" routerLinkActive=\"active\" >Customer List</a>\r\n                   <a mat-list-item routerLink=\"/5th\" routerLinkActive=\"active\" >5th</a>\r\n                   <a mat-list-item routerLink=\"/2nd\" routerLinkActive=\"active\" >6th</a>\r\n                   <a mat-list-item routerLink=\"/hero\" routerLinkActive=\"active\" >hero</a>\r\n                   <a mat-list-item routerLink=\"/pannel\" routerLinkActive=\"active\" >ExpentionPannel</a> -->\r\n               </mat-nav-list>\r\n        </mat-expansion-panel>\r\n       </mat-nav-list>\r\n     </mat-sidenav>\r\n \r\n     <!-- body part -->\r\n     <mat-sidenav-content class=\"matsidenavcontendt\" style=\"padding:10px;\">\r\n         <mat-card>\r\n           <!-- body -->\r\n           <router-outlet></router-outlet>\r\n         </mat-card>\r\n     </mat-sidenav-content>\r\n\r\n   </mat-sidenav-container>\r\n </div>\r\n"

/***/ }),

/***/ "./src/app/guard/admin-template/admin-template.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/guard/admin-template/admin-template.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-icon {\n  padding: 0 20px;\n  cursor: pointer; }\n\n.example-spacer {\n  flex: 1 1 auto; }\n\n.example-sidenav-container {\n  background-color: rgba(19, 22, 22, 0.219); }\n\n.mat-icon {\n  cursor: pointer; }\n\nmat-nav-list a {\n  height: 35px; }\n\n.exampl-mat-nav-list a {\n  font-size: 12px; }\n\n.matsidenavcontendt {\n  padding: 0%; }\n\n.example-container {\n  display: flex;\n  flex-direction: column;\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0; }\n\n.example-sidenav {\n  display: flex;\n  width: 200px;\n  background: rgba(122, 124, 243, 0.5); }\n\n.example-is-mobile .example-toolbar {\n  position: fixed;\n  /* Make sure the toolbar will stay on top of the content as it scrolls past. */\n  z-index: 2; }\n\n.matsidenav {\n  display: flex; }\n\n.example-sidenav-container {\n  /* When the sidenav is not fixed, stretch the sidenav container to fill the available space. This\r\n        causes `<mat-sidenav-content>` to act as our scrolling element for desktop layouts. */\n  flex: 1;\n  position: relative; }\n\n.example-is-mobile .example-sidenav-container {\n  /* When the sidenav is fixed, don't constrain the height of the sidenav container. This allows the\r\n        `<body>` to be our scrolling element for mobile layouts. */\n  flex: 1; }\n"

/***/ }),

/***/ "./src/app/guard/admin-template/admin-template.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/guard/admin-template/admin-template.component.ts ***!
  \******************************************************************/
/*! exports provided: AdminTemplateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminTemplateComponent", function() { return AdminTemplateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth/auth.guard */ "./src/app/guard/auth/auth.guard.ts");
/* harmony import */ var _global_service_global_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../global-service/global-service.service */ "./src/app/guard/global-service/global-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminTemplateComponent = /** @class */ (function () {
    function AdminTemplateComponent(changeDetectorRef, media, router, auth, globalService) {
        this.router = router;
        this.auth = auth;
        this.globalService = globalService;
        this.islogin = true;
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = function () { return changeDetectorRef.detectChanges(); };
        this.mobileQuery.addListener(this._mobileQueryListener);
    }
    AdminTemplateComponent.prototype.ngOnDestroy = function () {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    };
    AdminTemplateComponent.prototype.ngOnInit = function () {
    };
    AdminTemplateComponent.prototype.LogOut = function () {
        localStorage.clear();
        this.globalService.SetLocalValues();
        this.router.navigate(['/home']);
    };
    AdminTemplateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-template',
            template: __webpack_require__(/*! ./admin-template.component.html */ "./src/app/guard/admin-template/admin-template.component.html"),
            styles: [__webpack_require__(/*! ./admin-template.component.scss */ "./src/app/guard/admin-template/admin-template.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__["MediaMatcher"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _auth_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"],
            _global_service_global_service_service__WEBPACK_IMPORTED_MODULE_4__["GlobalServiceService"]])
    ], AdminTemplateComponent);
    return AdminTemplateComponent;
}());



/***/ }),

/***/ "./src/app/guard/auth-service/auth.service.ts":
/*!****************************************************!*\
  !*** ./src/app/guard/auth-service/auth.service.ts ***!
  \****************************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _login_login_service_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/login-service/login.service */ "./src/app/guard/login/login-service/login.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthService = /** @class */ (function () {
    function AuthService(http, loginService) {
        this.http = http;
        this.loginService = loginService;
    }
    AuthService.prototype.GetUserCrendials = function (username, password) {
        return this.http.get('');
    };
    AuthService.prototype.IsLogedIn = function () {
        var token = localStorage.getItem(this.loginService.LoginUserToken);
        if (token)
            return true;
        else
            return false;
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _login_login_service_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/guard/auth/auth.guard.ts":
/*!******************************************!*\
  !*** ./src/app/guard/auth/auth.guard.ts ***!
  \******************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _global_service_global_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../global-service/global-service.service */ "./src/app/guard/global-service/global-service.service.ts");
/* harmony import */ var _login_login_service_login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login/login-service/login.service */ "./src/app/guard/login/login-service/login.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, loginService, globalService) {
        this.router = router;
        this.loginService = loginService;
        this.globalService = globalService;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        // debugger;
        var token = localStorage.getItem(this.loginService.LoginUserToken);
        if (token) {
            this.globalService.SetLocalValues();
            return true;
        }
        else {
            this.globalService.SetLocalValues();
            this.router.navigate(['/login']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _login_login_service_login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"],
            _global_service_global_service_service__WEBPACK_IMPORTED_MODULE_2__["GlobalServiceService"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/guard/global-service/global-service.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/guard/global-service/global-service.service.ts ***!
  \****************************************************************/
/*! exports provided: GlobalServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalServiceService", function() { return GlobalServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GlobalServiceService = /** @class */ (function () {
    function GlobalServiceService() {
        this.BaseUrl = "http://localhost:60584/";
        this.globalScope = {
            userName: 'sujith',
            isLogedIn: false,
            UserToken: 'tokens'
        };
        this.LoginStatus = 'LoginStatus';
        this.LoginUserName = 'UserName';
        this.LoginUserInfo = 'UserInfo';
        this.LoginUserToken = 'UserToken';
    }
    GlobalServiceService.prototype.SetValue = function (value) {
        this.globalScope.isLogedIn = value;
    };
    GlobalServiceService.prototype.SetLocalValues = function () {
        this.globalScope.isLogedIn = localStorage.getItem(this.LoginStatus) == 'true' ? true : false;
        this.globalScope.userName = localStorage.getItem(this.LoginUserName);
        this.globalScope.UserToken = localStorage.getItem(this.LoginUserToken);
    };
    GlobalServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], GlobalServiceService);
    return GlobalServiceService;
}());



/***/ }),

/***/ "./src/app/guard/login/login-service/login.service.ts":
/*!************************************************************!*\
  !*** ./src/app/guard/login/login-service/login.service.ts ***!
  \************************************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _global_service_global_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../global-service/global-service.service */ "./src/app/guard/global-service/global-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginService = /** @class */ (function () {
    function LoginService(http, globalService) {
        this.http = http;
        this.globalService = globalService;
        this.LoginStatus = 'LoginStatus';
        this.LoginUserName = 'UserName';
        this.LoginUserInfo = 'UserInfo';
        this.LoginUserToken = 'UserToken';
    }
    LoginService.prototype.UserLogin = function (username, passwod) {
        var _this = this;
        return this.http.get(this.globalService.BaseUrl + 'api/Login?UserName=' + username + '&Password=' + passwod)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (err) { return _this.ErrorHandler(err); }));
    };
    LoginService.prototype.ErrorHandler = function (value) {
        return value;
    };
    LoginService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_service_global_service_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServiceService"]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/guard/login/login.component.html":
/*!**************************************************!*\
  !*** ./src/app/guard/login/login.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<mat-card class=\"mat-class\">\r\n  <form #login=\"ngForm\">\r\n      <mat-form-field>\r\n          <input matInput type=\"text\" name=\"username\" [(ngModel)]=\"login.username\" placeholder=\"User Name\">\r\n        </mat-form-field>  <br>\r\n\r\n        <mat-form-field>\r\n            <input matInput type=\"password\" name=\"password\" [(ngModel)]=\"login.password\" placeholder=\"password\">\r\n        </mat-form-field><br>\r\n\r\n        <button (click)=\"loginForm(login)\" mat-raised-button color=\"primary\">Login</button>\r\n  </form>\r\n</mat-card>"

/***/ }),

/***/ "./src/app/guard/login/login.component.scss":
/*!**************************************************!*\
  !*** ./src/app/guard/login/login.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-class {\n  font: 1em sans-serif;\n  font-size: 15px;\n  height: 189px;\n  width: 248px;\n  margin: 155px -22px 49px 532px; }\n"

/***/ }),

/***/ "./src/app/guard/login/login.component.ts":
/*!************************************************!*\
  !*** ./src/app/guard/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth-service/auth.service */ "./src/app/guard/auth-service/auth.service.ts");
/* harmony import */ var _global_service_global_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global-service/global-service.service */ "./src/app/guard/global-service/global-service.service.ts");
/* harmony import */ var _login_service_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login-service/login.service */ "./src/app/guard/login/login-service/login.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, authService, globalService, loginService, snakbar, http) {
        this.router = router;
        this.authService = authService;
        this.globalService = globalService;
        this.loginService = loginService;
        this.snakbar = snakbar;
        this.http = http;
        this.LoginStatus = 'LoginStatus';
        this.LoginUserName = 'UserName';
        this.LoginUserInfo = 'UserInfo';
        this.LoginUserToken = 'UserToken';
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.loginForm = function (ngform) {
        debugger;
        console.log(ngform);
        if (ngform.username && ngform.password) {
            var username = ngform.username;
            var password = ngform.password;
            localStorage.setItem(this.LoginStatus, 'true');
            localStorage.setItem(this.LoginUserName, 'user');
            localStorage.setItem(this.LoginUserToken, '8659864321687984561684');
            localStorage.setItem(this.LoginUserInfo, 'user, emial, token');
            this.globalService.SetLocalValues();
            // this.globalService.SetValue(true);
            this.router.navigate(['/employees']);
            // this.loginService.UserLogin(username, password)
            // this.http.get('http://localhost:60584/api/UserLogin?UserName='+ username +'&Password=' + password)
            //   .subscribe((data:any)=>{
            //     debugger
            //     if(data.status){
            //       localStorage.setItem(this.LoginStatus, data.status);
            //       localStorage.setItem(this.LoginUserName, data.UserInfo.UserLogin.UserName);
            //       localStorage.setItem(this.LoginUserToken, data.UserInfo.tocken);
            //       localStorage.setItem(this.LoginUserInfo, JSON.stringify(data.UserInfo))
            //       this.globalService.SetLocalValues();
            //       this.router.navigate(['/employees'])
            //     }
            //   })
        }
        else {
            this.snakbar.open('Username or Password are invalid!', 'close', {
                duration: 5000
            });
        }
        // this.globalService.SetValue(true);
        // localStorage.setItem('token', 'sdfasdf')
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/guard/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/guard/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _global_service_global_service_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServiceService"],
            _login_service_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/guard/material/material.module.ts":
/*!***************************************************!*\
  !*** ./src/app/guard/material/material.module.ts ***!
  \***************************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/esm5/sidenav.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm5/snack-bar.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/esm5/divider.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCheckboxModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
                _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_5__["MatSidenavModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_6__["MatListModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_8__["MatCardModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_9__["MatExpansionModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
                _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_11__["MatSnackBarModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_12__["MatInputModule"],
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_13__["MatPaginatorModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_14__["MatGridListModule"],
                _angular_material_divider__WEBPACK_IMPORTED_MODULE_15__["MatDividerModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_16__["MatTableModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_17__["MatSelectModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_19__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatNativeDateModule"],
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_20__["MatAutocompleteModule"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCheckboxModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
                _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_5__["MatSidenavModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_6__["MatListModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_8__["MatCardModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_9__["MatExpansionModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
                _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_11__["MatSnackBarModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_12__["MatInputModule"],
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_13__["MatPaginatorModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_14__["MatGridListModule"],
                _angular_material_divider__WEBPACK_IMPORTED_MODULE_15__["MatDividerModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_16__["MatTableModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_17__["MatSelectModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_19__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatNativeDateModule"],
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_20__["MatAutocompleteModule"]
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/organization/enquiry/enquiry.component.html":
/*!*************************************************************!*\
  !*** ./src/app/organization/enquiry/enquiry.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  enquiry works! sujith again changed by me\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/organization/enquiry/enquiry.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/organization/enquiry/enquiry.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/organization/enquiry/enquiry.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/organization/enquiry/enquiry.component.ts ***!
  \***********************************************************/
/*! exports provided: EnquiryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnquiryComponent", function() { return EnquiryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EnquiryComponent = /** @class */ (function () {
    function EnquiryComponent() {
    }
    EnquiryComponent.prototype.ngOnInit = function () {
    };
    EnquiryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-enquiry',
            template: __webpack_require__(/*! ./enquiry.component.html */ "./src/app/organization/enquiry/enquiry.component.html"),
            styles: [__webpack_require__(/*! ./enquiry.component.scss */ "./src/app/organization/enquiry/enquiry.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EnquiryComponent);
    return EnquiryComponent;
}());



/***/ }),

/***/ "./src/app/organization/job-setup/job-setup.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/organization/job-setup/job-setup.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  job-setup works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/organization/job-setup/job-setup.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/organization/job-setup/job-setup.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/organization/job-setup/job-setup.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/organization/job-setup/job-setup.component.ts ***!
  \***************************************************************/
/*! exports provided: JobSetupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobSetupComponent", function() { return JobSetupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var JobSetupComponent = /** @class */ (function () {
    function JobSetupComponent() {
    }
    JobSetupComponent.prototype.ngOnInit = function () {
    };
    JobSetupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-job-setup',
            template: __webpack_require__(/*! ./job-setup.component.html */ "./src/app/organization/job-setup/job-setup.component.html"),
            styles: [__webpack_require__(/*! ./job-setup.component.scss */ "./src/app/organization/job-setup/job-setup.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], JobSetupComponent);
    return JobSetupComponent;
}());



/***/ }),

/***/ "./src/app/organization/look-up/look-up.component.html":
/*!*************************************************************!*\
  !*** ./src/app/organization/look-up/look-up.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\r\n  <mat-toolbar-row>\r\n    <span>Third Line</span>\r\n    <span class=\"example-spacer\"></span>\r\n    <mat-icon class=\"example-icon\">favorite</mat-icon>\r\n    <mat-icon class=\"example-icon\">delete</mat-icon>\r\n  </mat-toolbar-row>\r\n</mat-toolbar>"

/***/ }),

/***/ "./src/app/organization/look-up/look-up.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/organization/look-up/look-up.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-icon {\n  padding: 0 19px; }\n\n.example-spacer {\n  flex: 1 1 auto; }\n"

/***/ }),

/***/ "./src/app/organization/look-up/look-up.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/organization/look-up/look-up.component.ts ***!
  \***********************************************************/
/*! exports provided: LookUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LookUpComponent", function() { return LookUpComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LookUpComponent = /** @class */ (function () {
    function LookUpComponent() {
    }
    LookUpComponent.prototype.ngOnInit = function () {
    };
    LookUpComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-look-up',
            template: __webpack_require__(/*! ./look-up.component.html */ "./src/app/organization/look-up/look-up.component.html"),
            styles: [__webpack_require__(/*! ./look-up.component.scss */ "./src/app/organization/look-up/look-up.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LookUpComponent);
    return LookUpComponent;
}());



/***/ }),

/***/ "./src/app/organization/menus/menus.component.html":
/*!*********************************************************!*\
  !*** ./src/app/organization/menus/menus.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  menus works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/organization/menus/menus.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/organization/menus/menus.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/organization/menus/menus.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/organization/menus/menus.component.ts ***!
  \*******************************************************/
/*! exports provided: MenusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenusComponent", function() { return MenusComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MenusComponent = /** @class */ (function () {
    function MenusComponent() {
    }
    MenusComponent.prototype.ngOnInit = function () {
    };
    MenusComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menus',
            template: __webpack_require__(/*! ./menus.component.html */ "./src/app/organization/menus/menus.component.html"),
            styles: [__webpack_require__(/*! ./menus.component.scss */ "./src/app/organization/menus/menus.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MenusComponent);
    return MenusComponent;
}());



/***/ }),

/***/ "./src/app/organization/notification/notification.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/organization/notification/notification.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  notification works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/organization/notification/notification.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/organization/notification/notification.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/organization/notification/notification.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/organization/notification/notification.component.ts ***!
  \*********************************************************************/
/*! exports provided: NotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationComponent", function() { return NotificationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NotificationComponent = /** @class */ (function () {
    function NotificationComponent() {
    }
    NotificationComponent.prototype.ngOnInit = function () {
    };
    NotificationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notification',
            template: __webpack_require__(/*! ./notification.component.html */ "./src/app/organization/notification/notification.component.html"),
            styles: [__webpack_require__(/*! ./notification.component.scss */ "./src/app/organization/notification/notification.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NotificationComponent);
    return NotificationComponent;
}());



/***/ }),

/***/ "./src/app/organization/pay-group/pay-group.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/organization/pay-group/pay-group.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  pay-group works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/organization/pay-group/pay-group.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/organization/pay-group/pay-group.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/organization/pay-group/pay-group.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/organization/pay-group/pay-group.component.ts ***!
  \***************************************************************/
/*! exports provided: PayGroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayGroupComponent", function() { return PayGroupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PayGroupComponent = /** @class */ (function () {
    function PayGroupComponent() {
    }
    PayGroupComponent.prototype.ngOnInit = function () {
    };
    PayGroupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pay-group',
            template: __webpack_require__(/*! ./pay-group.component.html */ "./src/app/organization/pay-group/pay-group.component.html"),
            styles: [__webpack_require__(/*! ./pay-group.component.scss */ "./src/app/organization/pay-group/pay-group.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PayGroupComponent);
    return PayGroupComponent;
}());



/***/ }),

/***/ "./src/app/organization/permission/permission.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/organization/permission/permission.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  permission works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/organization/permission/permission.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/organization/permission/permission.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/organization/permission/permission.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/organization/permission/permission.component.ts ***!
  \*****************************************************************/
/*! exports provided: PermissionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionComponent", function() { return PermissionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PermissionComponent = /** @class */ (function () {
    function PermissionComponent() {
    }
    PermissionComponent.prototype.ngOnInit = function () {
    };
    PermissionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-permission',
            template: __webpack_require__(/*! ./permission.component.html */ "./src/app/organization/permission/permission.component.html"),
            styles: [__webpack_require__(/*! ./permission.component.scss */ "./src/app/organization/permission/permission.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PermissionComponent);
    return PermissionComponent;
}());



/***/ }),

/***/ "./src/app/organization/roles/roles.component.html":
/*!*********************************************************!*\
  !*** ./src/app/organization/roles/roles.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  roles works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/organization/roles/roles.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/organization/roles/roles.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/organization/roles/roles.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/organization/roles/roles.component.ts ***!
  \*******************************************************/
/*! exports provided: RolesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolesComponent", function() { return RolesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RolesComponent = /** @class */ (function () {
    function RolesComponent() {
    }
    RolesComponent.prototype.ngOnInit = function () {
    };
    RolesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-roles',
            template: __webpack_require__(/*! ./roles.component.html */ "./src/app/organization/roles/roles.component.html"),
            styles: [__webpack_require__(/*! ./roles.component.scss */ "./src/app/organization/roles/roles.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], RolesComponent);
    return RolesComponent;
}());



/***/ }),

/***/ "./src/app/organization/staff/staff.component.html":
/*!*********************************************************!*\
  !*** ./src/app/organization/staff/staff.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  staff works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/organization/staff/staff.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/organization/staff/staff.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/organization/staff/staff.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/organization/staff/staff.component.ts ***!
  \*******************************************************/
/*! exports provided: StaffComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffComponent", function() { return StaffComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StaffComponent = /** @class */ (function () {
    function StaffComponent() {
    }
    StaffComponent.prototype.ngOnInit = function () {
    };
    StaffComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-staff',
            template: __webpack_require__(/*! ./staff.component.html */ "./src/app/organization/staff/staff.component.html"),
            styles: [__webpack_require__(/*! ./staff.component.scss */ "./src/app/organization/staff/staff.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StaffComponent);
    return StaffComponent;
}());



/***/ }),

/***/ "./src/app/organization/users/users.component.html":
/*!*********************************************************!*\
  !*** ./src/app/organization/users/users.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  users works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/organization/users/users.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/organization/users/users.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/organization/users/users.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/organization/users/users.component.ts ***!
  \*******************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UsersComponent = /** @class */ (function () {
    function UsersComponent() {
    }
    UsersComponent.prototype.ngOnInit = function () {
    };
    UsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! ./users.component.html */ "./src/app/organization/users/users.component.html"),
            styles: [__webpack_require__(/*! ./users.component.scss */ "./src/app/organization/users/users.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\angular\git-remote-work\demotwo\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map